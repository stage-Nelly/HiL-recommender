# VizLIRIS

L'outil VizLIRIS permet de recommander ou de regrouper des IRIS, par exemple dans le cadre d'une mobilité professionnelle.

Les [IRIS](https://www.insee.fr/fr/metadonnees/definition/c1523) sont des zones administratives fraçaises (un peu comparables à des quartiers). Les ~50 000 IRIS pour la France sont stockés au format GeoJSON.

La recommandation et le regroupement sont basés sur différents algorithmes : comparaison directe (mesure cosine), construction d'un profil utilisateur à partir de plusieurs IRIS appréciés (écart-type), des algorithmes de regroupement ([scikit-learn](https://scikit-learn.org/) clustering) et de la classification à partir d'IRIS considérés comme pertinents (one-class SVM). 

## Pré-requis

- Python, version >=3
- [MongoDB](https://www.mongodb.com/), version >=4, pour lequel il faudra importer la base de données des IRIS (cf installation).

## Installation

Pour installer VizLIRIS, taper dans un terminal :

```
python3 -m pip install -e vizliris/ --process-dependency-links
```

Cette commande installe les dépendances, dont [mongiris](https://gitlab.liris.cnrs.fr/fduchate/mongiris) qui permet l'interrogation d'une base de données sous MongoDB contenant les IRIS.

Il est donc nécessaire de crééer la base de données avec la collection d'IRIS. Pour cela, exécuter la commande (depuis le répertoire des exécutables de MongoDB si besoin) :

```
./mongorestore --archive=/path/to/dump-iris.bin
```

où `/path/to/` représente le chemin vers le fichier dump de la collection des IRIS (fourni de base avec le package mongiris dans `mongiris/data/dump-iris.bin`)

## Lancement de l'interface

Pour lancer VizLIRIS, taper dans un terminal :

```
python3 web_server.py
```

Après quelques informations, le terminal affiche l'URL permettant de tester VizLIRIS : `http://localhost:8080/`

## Crédits

Source de données : [INSEE](https://www.insee.fr/)

Contributeurs : laboratoire [LIRIS](https://liris.cnrs.fr/), laboratoire [CMW](https://www.centre-max-weber.fr/) et Labex [Intelligence des Mondes Urbains (IMU)](http://imu.universite-lyon.fr/projet/hil)