#!/usr/bin/env python
import os
import sys
from setuptools import setup
import glob
import shutil

# python3 -m setup bdist_wheel sdist
# python3 -m pip install -e ../mongiris/
# installation: python -m pip install -e ../vizliris/ --process-dependency-links

def delete_dir(directories):  # delete directories (ignore errors such as read-only files)
    for directory in directories:
        shutil.rmtree(directory, ignore_errors=True)
# delete build/config directories because egg-info only updates config files for new versions
delete_dir(['./vizliris.egg-info/', './build/', './dist/'])  

if sys.argv[-1] == "publish":
	os.system("python setup.py bdist_wheel upload --sign")
	sys.exit()

readme = open("README.md").read()

setup(long_description=readme)

