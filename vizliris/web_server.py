#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
#   Main script for Flask server, with routes
#   http://flask.pocoo.org/
# =============================================================================

from flask import Flask, url_for, render_template, request, jsonify, send_from_directory
import os
import json
import random
from vizliris import config
from vizliris import recommender
from mongiris import Mongiris
import sys

app = Flask(__name__)

# main variables
current_arrivals = None  # features for all IRIS (start + work)

# connection to the IRIS collection in MongoDB
db = Mongiris()
db.init_connection()

# list_features = config.indicators_ids
# list_features = config.indicators_raw_ids
# list_features_file = config.grouping_indicators_file
# list_features = config.geojson_indicators_label


def get_list_features(list_iris):
    """
    This method extracts and returns the list of indicators (raw or grouped) for the list of iris.
    :param list_iris: a list of iris (geojson format)
    :return: a list of features (indicators) to be used with recommending or clustering algorithms
    """
    nb_all_zeros = 0  # number of iris whose grouped indicators all have value 0.0
    nb_iris = len(list_iris)
    list_features = list()
    for iris in list_iris:
        all_zeros = True
        for (key, value) in iris["properties"][config.geojson_grouped_indicators_label].items():
            if value != 0.0:
                all_zeros = False
        if all_zeros:
            nb_all_zeros += 1
    if nb_all_zeros > (nb_iris/2):  # heuristic, if more than half iris do not have values for grouped ind, use raw ind
        list_features = config.indicators_raw_ids
    else:  # else (sufficiently iris with values for grouped indicators), use grouped indicators
        list_features = list_iris[0]["properties"][config.geojson_grouped_indicators_label].keys()
    return list_features


@app.route('/', defaults={'page': None})
@app.route('/<page>', methods=["GET", "POST"])
def index(page):
    if request.method == "POST":
        if page is None:
            return render_template('index.html')
        elif page.find('recommendation.html') > -1:  # there is an argument POST
            return render_template('recommendation.html')
        elif page.find('clustering.html') > -1:  # there is an argument POST
            return render_template('clustering.html')
        else:
            return render_template(page)
    else:  # to avoid : Method Not Allowed The method is not allowed for the requested URL.
        return render_template('index.html')


@app.route('/favicon.ico')
@app.route('/<page>/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico', mimetype='image/favicon.ico')


@app.route('/geocoding', methods=['GET'], strict_slashes=False)
def geocoding():
    lat = request.args['lat']
    lng = request.args['lng']
    iris = db.point_in_which_iris([float(lng), float(lat)], {"properties.CODE_IRIS": 1})
    if iris is not None:
        return iris["properties"]["CODE_IRIS"]
    return None


@app.route('/cosine', methods=['GET'], strict_slashes=False)  # strict_slashes for problem with url with trailing '/'
def do_cosine():
    try:
        # getting each GET parameter
        code_start_iris = request.args['code_iris']
        codes_candidates = request.args['candidates']
        topk = int(request.args['top'])

        # getting each candidate iris and the start iris
        split_codes = codes_candidates.split("_")
        candidates = []
        for code in split_codes:  # get each candidate IRIS and store it in candidates
            iris = db.find_one_document(db.iris_collection, {"properties.CODE_IRIS": code})
            candidates.append(iris)
        start_iris = db.find_one_document(db.iris_collection, {"properties.CODE_IRIS": code_start_iris})

        # start recommendation
        list_features = get_list_features([start_iris])
        recommendations, justifications = recommender.recommend_cosine_measure(start_iris, candidates,
                                                                               list_features, topk)
        return json.dumps(
            {'status': 'OK', 'recommendations': recommendations, 'justifications': justifications,
             'current_arrivals': candidates, 'current_departures': start_iris, 'typeAlgo': 'cosine'})
    except:
        return str(sys.exc_info()[0])


@app.route('/deviation', methods=['GET'], strict_slashes=False)  # strict_slashes for problem with url with trailing '/'
def do_deviation():
    try:

        # getting each GET parameter
        codes_starts = request.args['codes_iris']
        codes_candidates = request.args['candidates']
        topk = int(request.args['top'])

        # getting each start IRIS
        split_codes_starts = codes_starts.split("_")  # split each code (i.e. code1_code2_code3...)
        split_codes_starts = split_codes_starts[1:]  # remove the first code start which is actually the end (!)
        starts_iris = []  # store the IRIS from the split
        for code in split_codes_starts:
            start_iris = db.find_one_document(db.iris_collection, {"properties.CODE_IRIS": code})  # get each IRIS
            starts_iris.append(start_iris)

        # getting each candidate
        split_codes_cand = codes_candidates.split("_")
        candidates = []
        for code in split_codes_cand:  # get each candidate IRIS and store it in candidates
            cand_iris = db.find_one_document(db.iris_collection, {"properties.CODE_IRIS": code})
            candidates.append(cand_iris)

        # start recommendation
        list_features = get_list_features(starts_iris)
        recommendations = recommender.recommend_standard_deviation(starts_iris, candidates, list_features, topk)
        return json.dumps(
            {'status': 'OK', 'recommendations': recommendations, 'current_arrivals': candidates,
             'current_departures': starts_iris, 'typeAlgo': 'std'})
    except:
        return str(sys.exc_info()[0])


@app.route('/clusteringReco', methods=['GET'], strict_slashes=False)  # strict_slashes for problem with url with trailing '/'
def do_clustering_r():
    # getting each GET parameter
    code_iris_prediction = request.args['code_iris']
    codes_candidates = request.args['candidates']
    topk = int(request.args['top'])
    algo_clustering = request.args['algo_clust']

    # getting each candidate iris and the start iris
    split_codes = codes_candidates.split("_")
    candidates = []
    for code in split_codes:  # get each candidate IRIS and store it in candidates
        iris = db.find_one_document(db.iris_collection, {"properties.CODE_IRIS": code})
        candidates.append(iris)
    start_iris = db.find_one_document(db.iris_collection, {"properties.CODE_IRIS": code_iris_prediction})

    # start recommendation
    list_features = get_list_features([start_iris])
    recommendations, nb_clusters, justifications = recommender.recommend_clustering(candidates,
                                                                                    list_features, start_iris,
                                                                                    algo_clustering, topk)
    return json.dumps({'status': 'OK', 'recommendations': recommendations, 'justifications': justifications,
                       'nb_clusters': nb_clusters, 'current_arrivals': candidates, 'typeAlgo': 'clustering',
                       'current_departures': start_iris})


@app.route('/recommendHdbscan', methods=['GET'], strict_slashes=False)  # strict_slashes for problem with url with trailing '/'
def do_recommend_hdbscan():
    # getting each GET parameter
    code_iris_prediction = request.args['code_iris']
    codes_candidates = request.args['candidates']
    topk = int(request.args['top'])

    # getting each candidate iris and the start iris
    split_codes = codes_candidates.split("_")
    candidates = []
    for code in split_codes:  # get each candidate IRIS and store it in candidates
        iris = db.find_one_document(db.iris_collection, {"properties.CODE_IRIS": code})
        candidates.append(iris)
    start_iris = db.find_one_document(db.iris_collection, {"properties.CODE_IRIS": code_iris_prediction})

    # start recommendation
    list_features = get_list_features([start_iris])
    recommendations, nb_clusters = recommender.recommend_hdbscan(candidates, list_features, start_iris,)
    return json.dumps({'status': 'OK', 'recommendations': recommendations, 'nb_clusters': nb_clusters,
                       'current_arrivals': candidates, 'typeAlgo': 'clustering', 'current_departures': start_iris})


@app.route('/svm', methods=['GET'], strict_slashes=False)  # strict_slashes for problem with url with trailing '/'
def do_svm():
    # getting each GET parameter
    code_iris_prediction = request.args['code_iris']
    codes_candidates = request.args['candidates']
    #topk = int(request.args['top'])
    algo_svm = 'one_class_svm'  # request.args['algo_svm']

    # getting each candidate iris and the start iris
    split_codes = codes_candidates.split("_")
    candidates = []
    for code in split_codes:  # get each candidate IRIS and store it in candidates
        iris = db.find_one_document(db.iris_collection, {"properties.CODE_IRIS": code})
        candidates.append(iris)
    start_iris = db.find_one_document(db.iris_collection, {"properties.CODE_IRIS": code_iris_prediction})

    # start recommendation
    list_features = get_list_features([start_iris])
    recommendations = recommender.recommend_svm(start_iris, candidates, list_features, algo_svm)
    return json.dumps(
        {'status': 'OK', 'recommendations': recommendations, 'current_arrivals': candidates,
         'current_departures': start_iris, 'typeAlgo': 'svm'})


@app.route('/clusteringClust', methods=['GET'], strict_slashes=False)  # strict_slashes for problem with url with trailing '/'
def do_clustering_c():
    # getting each GET parameter
    codes_candidates = request.args['candidates']
    topk = 10
    algo_clustering = request.args['algo_clust']

    # getting each candidate iris for clustering
    split_codes = codes_candidates.split("_")
    candidates = []
    for code in split_codes:  # get each candidate IRIS and store it in candidates
        iris = db.find_one_document(db.iris_collection, {"properties.CODE_IRIS": code})
        candidates.append(iris)

    # start recommendation
    list_features = get_list_features(candidates)
    if(algo_clustering == 'HDBSCAN'):  # some specific parameters for HDBSCAN
        min_clusters = 3  # minimum number of iris per cluster
        min_samples = 1 # int(len(candidates)/100)  # more noise (unclassified iris, or class -1) with large values
        recommendations, nb_clusters = recommender.recommend_hdbscan(candidates, list_features,
                                                                     min_cluster_size=min_clusters,
                                                                     min_samples=min_samples)
    else:
        recommendations, nb_clusters, _ = recommender.recommend_clustering(candidates, list_features, None, algo_clustering, topk)
    return json.dumps({'status': 'OK', 'recommendations': recommendations, 'typeAlgo': 'clustering',
                       'nb_clusters': nb_clusters, 'current_arrivals': candidates})


@app.route('/neighborhood', methods=['GET'], strict_slashes=False)
def build_neighborhood_iris():
    distance = 1000.0 * float(request.args['dist'])  # distance saisie en km, mais dans MongoDB en mètres
    code_start_iris = request.args['code_iris']
    start_iris = db.find_one_document(db.iris_collection, {"properties.CODE_IRIS": code_start_iris})
    #TODO: near only accepts a Point. For iris_start, which point to choose ? MongoDB has no "centroid" function.
    #TODO: here choosing a random point in geometry
    random_coord = random.choice(start_iris["geometry"]["coordinates"][0])  # get a random coordinate
    neighbors = db.near(db.iris_collection, random_coord, None, distance)
    return jsonify(neighbors)


if __name__ == "__main__":
    # app.debug = True
    app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0  # do not cache files, especially static files such as JS
    app.run(port=8080)
