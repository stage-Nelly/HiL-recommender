#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
#   XLS utilities, mainly for INSEE indicators (parsing Excel files, etc.) and mobilipass data from HiL
#   https://pypi.python.org/pypi/openpyxl/
#   https://xlrd.readthedocs.io/en/latest/api.html
# =============================================================================

from vizliris import config
import unittest
import sys
import xlrd  # for old Excel files (.xls) and new files (.xlsx Excel 2010)
#import openpyxl  # for new Excel files (.xlsx Excel 2010)


def parse_xls_to_dict(xls_file_path):
    """
    Parse an XLS file (excel/calc) produced by INSEE and containing indicators about IRIS.
    :param xls_file_path: the path to the XLS file to be parsed
    :return: short_fieldnames: a list containing field ID (or abbreviated field names)
    :return: long_fieldnames: a list containing the complete field names
    :return: indicators: a dictionary such as {id_iris1: {ind1: val1, ind2: val2, ...}, id_iris2: {ind1: val1, ind2: val2, ...}, ...}
    """
    logger = config.logging.getLogger(__name__)
    indicators = dict()
    try:
        wb = xlrd.open_workbook(xls_file_path,
                                ragged_rows=True)  # ragged_rows to True to avoid empty cells at the end of rows)
    except Exception as e:
        sys.exit('Error while parsing XLS file {}: {}'.format(xls_file_path, e))
    #sheet = wb.sheet_by_name("IRIS")  # data is stored in the sheet "IRIS", else wb.sheet_names()
    sheet = wb.sheet_by_index(0)  # sheet are sometimes called IRIS, also IRIS_DEC
    long_fieldnames = sheet.row_values(4)  # row 4 contains the long labels
    short_fieldnames = sheet.row_values(5)  # row 5 contains the short labels (usually not meaningful)
    nb_fields = len(short_fieldnames)
    for i in range(6, sheet.nrows, 1):
        iris_id = sheet.cell_value(i, 0)  # IRIS id is in the first column
        #print(sheet.row_values(i))
        if sheet.row_len(i) == nb_fields:  # some rows may not include all fields
            if iris_id not in indicators:
                indicators[iris_id] = dict()
            for j in range (0, nb_fields):
                field = short_fieldnames[j]  # + "  -  " + long_fieldnames[j]
                val = sheet.cell_value(i, j)
                indicators[iris_id][field] = val
        else:
            logger.warning("Ignored row (missing fields) : " + str(sheet.row_values(i)))
    return short_fieldnames, long_fieldnames, indicators


def parse_data_HiL_to_dict(xls_file_path):
    logger = config.logging.getLogger(__name__)
    data = dict()
    try:
        wb = xlrd.open_workbook(xls_file_path,
                                ragged_rows=True)  # ragged_rows to True to avoid empty cells at the end of rows)
    except Exception as e:
        sys.exit('Error while parsing XLS file {}: {}'.format(xls_file_path, e))
    sheet = wb.sheet_by_name("Mobilipass - 6") # ("Mobilipass")
    long_fieldnames = sheet.row_values(1)  # row 2 contains the long labels
    nb_fields = len(long_fieldnames)
    for i in range(2, sheet.nrows, 1):
        person_id = sheet.cell_value(i, 0)  # person id is in the first column
        if sheet.row_len(i) == nb_fields:  # some rows may not include all fields
            if person_id not in data:
                data[person_id] = dict()
            for j in range (0, nb_fields):
                field = long_fieldnames[j]
                val = sheet.cell_value(i, j)
                data[person_id][field] = val
        else:
            logger.warning("Ignored row: " + str(sheet.row_values(i)))
    return long_fieldnames, data


class TestCase(unittest.TestCase):

    def atest_parse_xls_to_dict(self):
        f = config.indicators_files[0]  # careful, no guarantee that the first file is always the same (new or deleted ones)
        _, _, records = parse_xls_to_dict(f)
        assert (len(records) == 14089), 'Error: expecting 14089 IRIS, extracted %i' % len(records)

    def atest_parse_all_xls_to_dict(self):
        for f in config.indicators_files:
            print("Parsing: " + f)
            parse_xls_to_dict(f)
        print("Done !")

    def test_parse_HiL_mobilipass(self):
        f = config.xls_data_HiL_file_path
        l, d = parse_data_HiL_to_dict(f)
        print(l)
        #print(d)


if __name__ == "__main__":
    unittest.main(verbosity=2)  # run all tests with verbose mode


