#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
#   CSV utilities (parsing INSEE files, etc.)
# =============================================================================

import csv
import config
import sys
import unittest


def parse_csv_to_dict(csv_file_path):
    records = dict()
    with open(csv_file_path, newline = '') as csv_file:  # newline = ''
        reader = csv.DictReader(csv_file)
        fieldnames = reader.fieldnames
        try:
            for row in reader:
                id_iris = row['IRIS']
                records[id_iris] = row
        except csv.Error as e:
            sys.exit('Error while parsing CSV file {}, line {}: {}'.format(csv_file_path, reader.line_num, e))
    return fieldnames, records


class TestCase(unittest.TestCase):

    def test_parse_csv_to_dict(self):
        file = config.csv_loisirs
        _, records = parse_csv_to_dict(file)
        assert (len(records) == 14096), 'Error: expecting 14096 records, extracted %i' % len(records)


'''
def integrate_leisures(iris_dict, indicators_csv):
    """
    OBSOLETE: csv needs a manuel conversion from xls, directly parse xls with integrate_xls_file()
    Integrate IRIS data with indicators (about IRIS) and produce a dict of IRIS.
    Careful : indicators are not available for all IRIS, and some indicators concern IRIS #69029ZZZZ (ZZZZ meaning the
    neighbourhood, not the IRIS).
    :param iris_dict: a dictionary with IRIS data
    :param indicators_csv: a csv file path containing INSEE indicators
    :return: a geojson merged dict that integrates both IRIS data and INSEE indicators
    """
    fieldnames, indicators = csv_utils.parse_csv_to_dict(indicators_csv)
    res_iris_dict = dict(iris_dict)
    for key, prop_values in indicators.items():
        for feature in res_iris_dict["features"]:
            if key == feature["properties"]["CODE_IRIS"]:  # indicator record concerns an iris
                if "indicateurs" not in feature["properties"]:
                    feature["properties"]["indicateurs"] = dict()
                for prop, value in prop_values.items():
                    if prop not in feature["properties"]["indicateurs"] and prop not in feature["properties"]:
                        feature["properties"]["indicateurs"][prop] = value
                break
    return res_iris_dict
'''


if __name__ == "__main__":
    unittest.main(verbosity=2)  # run all tests with verbose mode

