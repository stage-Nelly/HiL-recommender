#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
#   Configuration files (variables, logging format, etc.)
# =============================================================================

import logging
import warnings
import os
import random

app_name = "VizLIRIS"

logging.basicConfig(format='[%(levelname)s] - %(name)s - %(asctime)s : %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)
warnings.filterwarnings("ignore")
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=FutureWarning)

# directories
data_dir = 'data'
iris_dir = os.path.join(data_dir, 'iris')
communes_dir = os.path.join(data_dir, 'communes')
insee_dir = os.path.join(data_dir, 'insee')
departements_dir = os.path.join(data_dir, 'departements')
indicateurs_dir = os.path.join(data_dir, 'indicateurs')
expes_dir = os.path.join(data_dir, 'expes')
web_dir = os.path.join('static', 'data')

# geojson files
geojson_all_iris_file_path = os.path.join(iris_dir, 'contours-IRIS.geojson')
geojson_iris_69_file_path = os.path.join(iris_dir, 'contours-IRIS-69.geojson')
json_all_indexes_file_path = os.path.join(iris_dir, 'all_indexes-FR.json')
json_all_dictionaries_file_path = os.path.join(iris_dir, 'all_dictionaries-FR.json')
geojson_communes_metropole_file_path = os.path.join(communes_dir, 'communes-metropole.geojson')
geojson_integrated_output_file_FR = os.path.join(web_dir, 'iris-FR.geojson')
geojson_test_output_file = os.path.join(web_dir, 'test.geojson')
geojson_integrated_output_departement_dir = os.path.join(web_dir, 'iris_by_departments')
geojson_departments_FR = os.path.join(departements_dir, 'departements.geojson')
json_department_neighbors_FR = os.path.join(departements_dir, 'departement-voisins.json')
json_neighborhoods_FR = os.path.join(iris_dir, 'voisinages.json')

# example of geojson file (for Rhone department 69)
geojson_integrated_output_file_69 = os.path.join(geojson_integrated_output_departement_dir, '69.geojson')
geojson_index_69 = os.path.join(geojson_integrated_output_departement_dir, '69-index.json')

#geojson_iris_schema = {"IRIS", "NOM_IRIS", "TYP_IRIS", "INSEE_COM", "NOM_COM", "CODE_IRIS"}

# new labels for output geojson iris files and output json indicators dictionary (labels not in geojson standard)
geojson_indicators_label = 'raw_indicators'
geojson_insee_files_label = 'insee_files'
geojson_shortname_label = 'short_fieldname'
geojson_longname_label = 'long_fieldname'
geojson_grouped_indicators_label = 'grouped_indicators'
geojson_profiles_ideal_neighbourhood_1km_label = 'HiL_profiles_ideal_neighbours_1km'  # neighbors from iris_work to iris_arrival + 1km
geojson_profiles_ideal_neighbourhood_2km_label = 'HiL_profiles_ideal_neighbours_2km'  # neighbors from iris_work to iris_arrival + 2km
geojson_direct_neighbourhood_label = 'direct_neighbours'  # neighbours at level 1
geojson_direct_neighbourhood_grouped_indicators_label = 'direct_neighbours_grouped_indicators'
geojson_level2_neighbourhood_label = 'neighbours_level2'
geojson_level2_neighbourhood_grouped_indicators_label = 'level2_neighbours_grouped_indicators'
geojson_neighbourhood_500m_label = 'neighbours_500m'
geojson_neighbourhood_1000m_label = 'neighbours_1000m'
geojson_neighbourhood_2000m_label = 'neighbours_2000m'
geojson_neighbourhood_5000m_label = 'neighbours_5000m'
geojson_neighbourhood_500m_grouped_indicators_label = 'neighbours_500m_grouped_indicators'
geojson_neighbourhood_1000m_grouped_indicators_label = 'neighbours_1000m_grouped_indicators'
geojson_neighbourhood_2000m_grouped_indicators_label = 'neighbours_2000m_grouped_indicators'
geojson_neighbourhood_5000m_grouped_indicators_label = 'neighbours_5000m_grouped_indicators'

# insee indicateurs files
csv_loisirs = os.path.join(insee_dir, 'sport-loisirs-2016.csv')

# generate a list of INSEE XLSX files to be integrated
indicators_files = list()
for f in os.listdir(insee_dir):
    fp = os.path.join(insee_dir, f)
    if os.path.isfile(fp) and fp.endswith(".xls"):
        indicators_files.append(fp)

# json filepath containing formulas on how to group raw indicators
grouping_indicators_file = os.path.join(indicateurs_dir, "regroupement_indicateurs.json")

# generates a list of relevant indicators extracted from the manually created file config.indicators_list_file
indicators_list_file = os.path.join(indicateurs_dir, "liste-indicateurs.txt")  # a file containing short fieldname of relevant indicators
with open(indicators_list_file) as fic:
    indicators_ids = fic.readlines()
indicators_ids = [x.strip('\n') for x in indicators_ids]
indicators_raw_list_file = os.path.join(indicateurs_dir, "liste-indicateurs-2.txt")  # a file containing short fieldname of relevant indicators
with open(indicators_raw_list_file) as fic:
    indicators_raw_ids = fic.readlines()
indicators_raw_ids = [x.strip('\n') for x in indicators_raw_ids]
# main indicators do not have the same labels in IRIS files and indicator files, so manual intialization of these labels
indicators_dictionary_init = {'CODE_IRIS': {"long_fieldname": 'Code IRIS'}, 'NOM_IRIS': {"long_fieldname": 'Nom IRIS'},
                              'INSEE_COM': {"long_fieldname": 'Code postal commune'}, 'NOM_COM': {"long_fieldname": 'Nom commune'},
                              'TYP_IRIS': {"long_fieldname": 'Type IRIS'}, 'REG': {"long_fieldname": 'Code région'},
                              'DEP': {"long_fieldname": 'Code département'}}
# some properties (found in XLS INSEE files) are actually data about IRIS (and not indicators)
metadata_about_iris = ['DEP', 'REG', 'TRIRIS']

# list of recommendation algorithms
basic_recommendation_algorithms = ['cosine', ]
homemade_recommendation_algorithms = ['std', ]
clustering_recommendation_algorithms = ['kmeans', 'affinity', 'birch', 'dbscan', 'agglo', # 'featureagglo',
                                        'meanshift', 'minimbatchkmeans', 'spectral', ]
svm_recommendation_algorithms = ['one_class_svm', 'linear_svc', 'nu_svc', 'nu_svr', 'svr', ]

# data HiL structured and (partially) normalized by Aurelien
json_HiL_profils_from_to = os.path.join(expes_dir, 'profils_HiL_from_to.json')  # a manually created file for expes
results_expe_HiL = os.path.join(expes_dir, 'results_expe_HiL.json')
xls_data_HiL_file_path = os.path.join(data_dir, 'socio', 'tableau-mobilpass-2018-10-05.xlsx')
cache_expe_HiL_clustering_all_vectors_1km = os.path.join(expes_dir, 'cache_expe_HiL_clustering_all_vectors_1km.json')
max_bounding_box_iris69 = 18.01  # maximum bounding box of Rhone iris (Amplepuis Contour)


# returns a random number in [0, 1]
def get_random_number():
    return random.random()


def is_number(value):
    # check whether a string contains a number
    try:
        float(value)
        return True
    except ValueError:
        return False

# Hil profiles that need a manual correction (e.g., iris_work and iris_arrival are far)
profiles_HiL_corrected = {
    '000004': {'code_iris_arrival': '130390105', 'code_iris_start': '572690103', 'code_iris_work': '130390103'}, # iris_work en 76 (site Sollac)
    '000018': {'code_iris_arrival': '132020401', 'code_iris_start': '212311101', 'code_iris_work': '130390103'}, # iris_work en 76 (site Sollac)
    '000027': {'code_iris_arrival': '130560106', 'code_iris_start': '596060605', 'code_iris_work': '130390103'}, # iris_work en 76 (site Sollac)
    # '000031': {'code_iris_arrival': '693830703', 'code_iris_start': '841380102', 'code_iris_work': '841380103'}, # OK (ecole à Lyon)
    '000043': {'code_iris_arrival': '130470109', 'code_iris_start': '591831202', 'code_iris_work': '766720000'}, # iris_work en 76 (site Sollac)
    '000047': {}, # iris_work en 76 (site Sollac)
    '000048': {}, # iris_work en 76 (site Sollac)
}

list_grouped_indicators = ['logement-resident', 'education-superieur-prive', 'animation-culturel', 'education-secondaire-cycle1-public', 'education-secondaire-cycle2-professionnel-public', 'animation-commerce-nonalimentaire', 'education-primaire-prive', 'espacevert', 'service-sante', 'service-divers-public', 'education-secondaire-cycle2-professionnel-prive', 'education-secondaire-cycle2-general-public', 'education-creche', 'animation-divertissement', 'animation-commerce-alimentaire-proximite', 'csp', 'service-divers-prive', 'animation-commerce-alimentaire-grandesurface', 'logement-type', 'education-secondaire-cycle2-general-prive', 'transport-longuedistance', 'transport-busmetrotram', 'logement-annee', 'transport-velo', 'logement-residence', 'securite', 'education-secondaire-cycle1-prive', 'loisir', 'education-primaire-public', 'service-justice', 'service-emploi', 'education-superieur-public', 'service-actionsociale', ]
list_all_indicators = ['C14_ACTOCC15P_DROU', 'NB_F121_NB_AIREJEU', 'P14_F1529', 'P14_F6074', 'P14_RP_GRAT', 'P14_FETUD1564', 'NB_A406', 'NB_A405', 'NB_A404', 'NB_A403', 'P14_RPAPPART_ACH19', 'NB_A401', 'P14_RPMAISON_ACH90', 'P14_FNSCOL15P_SUP', 'NB_F110_NB_COU', 'NB_F104_NB_AIREJEU', 'NB_D221', 'P14_PMEN_ANEM10P', 'UU2010', 'P14_HNSCOL15P', 'C14_PMEN', 'P14_CHOM5564', 'P14_F2554', 'NB_F113_NB_AIREJEU', 'NB_F121', 'NB_F120', 'NB_A122', 'NB_F302_NB_SALLES', 'NB_A124', 'NB_A125', 'NB_D502', 'C14_PMEN_CS8', 'P14_MAISON', 'C14_PMEN_CS4', 'C14_PMEN_CS5', 'NB_F303_NB_SALLES', 'C14_PMEN_CS7', 'DEC_D814', 'C14_PMEN_CS1', 'C14_PMEN_CS2', 'C14_PMEN_CS3', 'P14_ACTOCC15P_ILT3', 'NB_E106', 'NB_E102', 'NB_E103', 'NB_E101', 'DEC_D314', 'P14_ACTOCC15P', 'C14_ACTOCC15P', 'P14_CHOM1524', 'C14_F15P', 'NB_F103_NB_ECL', 'P14_POP80P', 'P14_POP15P_NONMARIEE', 'NB_F118_NB_COU', 'NB_F114_NB_AIREJEU', 'NB_F114_NB_COU', 'C104_NB_CANT', 'P14_RPMAISON_ACHTOT', 'P14_FACT1524', 'P14_FINACT1564', 'P14_RPAPPART_ACH11', 'P14_ACT1564', 'P14_ACT2554', 'P14_ACTOCC15P_TP', 'P14_F5564', 'P14_ACTOCC2554', 'P14_MEN_ANEM10P', 'P14_H2554', 'C14_MEN_CS8', 'C14_MEN_CS5', 'P14_HACTOCC15P', 'C14_MEN_CS7', 'C14_MEN_CS6', 'C14_MEN_CS1', 'C14_MEN_CS3', 'NB_A402', 'GRD_QUART', 'NB_F121_NB_ECL', 'P14_SAL15P', 'P14_SCOL2529', 'NB_F106_NB_AIREJEU', 'P14_NSCOL15P_CAPBEP', 'C304_NB_INT', 'P14_MEN_ANEM0002', 'P14_RP_CCCOLL', 'P14_POP4054', 'NB_F116_NB_COU', 'P14_RP_100120M2', 'P14_NBPI_RP', 'P14_FACTOCC1564', 'DEC_PIMP14', 'P14_POP0610', 'P14_RETR1564', 'P14_POP2539', 'P14_POPMEN15P', 'P14_F1524', 'P14_RP_ACH05', 'P14_NPER_RP_GRAT', 'NB_F111_NB_AIREJEU', 'P14_RPMAISON_ACH70', 'NB_F104_NB_ECL', 'C14_F15P_CS4', 'C14_F15P_CS5', 'C14_F15P_CS6', 'C14_F15P_CS7', 'C14_F15P_CS1', 'C14_F15P_CS2', 'C14_F15P_CS3', 'P14_HAINACT1564', 'C14_ACTOCC15P_TCOM', 'C14_F15P_CS8', 'C201_NB_EP', 'P14_POP5579', 'P14_H6074', 'DEC_EQ14', 'P14_RPMAISON', 'NB_F106_NB_ECL', 'DEC_PAUT14', 'P14_ETUD1564', 'P14_ANEM_RP_LOCHLMV', 'NB_F111_NB_COU', 'DEC_D214', 'TRIRIS', 'P14_ANEM_RP', 'P14_POP6579', 'P14_RP_3040M2', 'DEC_PPEN14', 'NB_F108_NB_ECL', 'P14_F0019', 'P14_POP1824', 'P14_HNSCOL15P_BAC', 'P14_POPMEN1524', 'C14_PMEN_MENFAMMONO', 'C14_PMEN_MENCOUPAENF', 'P14_MEN', 'P14_ACT1524', 'P14_POP0205', 'NB_F202', 'P14_RP_5PP', 'C14_MENCOUPAENF', 'P14_RP_120M2P', 'C304', 'C305', 'C302', 'C303', 'C301', 'P14_RP_M30M2', 'P14_POP65P', 'P14_POP15P', 'P14_HRETR1564', 'NB_F109_NB_ECL', 'P14_FACTOCC2554', 'P14_SAL15P_EMPAID', 'DEC_S80S2014', 'P14_SAL15P_TP', 'LIBIRIS', 'NB_F116_NB_ECL', 'P14_NSAL15P_TP', 'C14_H15P', 'C401', 'C403', 'NB_F104_NB_COU', 'C409', 'NB_G101', 'NB_G102', 'NB_G103', 'NB_G104', 'P14_POPMEN80P', 'DEC_D414', 'DEC_PTSA14', 'P14_FACTOCC1524', 'NB_F101_NB_ECL', 'NB_F109_NB_AIREJEU', 'P14_F1564', 'NB_A208', 'P14_RP_ACH45', 'P14_RP_LOC', 'NB_A203', 'NB_A205', 'NB_A207', 'NB_A206', 'NB_B301', 'NB_B303', 'NB_B302', 'NB_B305', 'NB_B304', 'NB_B307', 'NB_B306', 'NB_B309', 'NB_B308', 'NB_F119_NB_ECL', 'NB_A120', 'P14_POP0014', 'NB_A121', 'C14_PMEN_MENHSEUL', 'C104_NB_PELEM', 'P14_POP0019', 'NB_F117_NB_AIREJEU', 'NB_D211', 'NB_D210', 'NB_D213', 'NB_A123', 'P14_RP_CHOS', 'NB_A115', 'P14_NPER_RP', 'NB_F107_NB_COU', 'P14_RPMAISON_ACH45', 'NB_F101_NB_COU', 'NB_F120_NB_AIREJEU', 'NB_A119', 'P14_RPAPPART', 'P14_RP', 'C14_POP15P', 'P14_CHOM2554', 'P14_NBPI_RPMAISON', 'C14_ACTOCC15P_VOIT', 'NB_F110_NB_AIREJEU', 'P14_SCOL1114', 'P14_NBPI_RPAPPART', 'MODIF_IRIS', 'P14_POP1529', 'NB_F119_NB_AIREJEU', 'P14_POP1524', 'C305_NB_INT', 'C105_NB_PELEM', 'P14_RP_ACH11', 'C14_MENHSEUL', 'C301_NB_EP', 'C14_PMEN_CS6', 'P14_HCHOM1564', 'C301_NB_PGE', 'C14_NE24F2', 'C14_NE24F3', 'C14_NE24F0', 'C14_NE24F1', 'P14_FACT5564', 'C102_NB_CANT', 'P14_H1564', 'DEC_Q114', 'NB_F120_NB_ECL', 'NB_D601', 'P14_ACT5564', 'P14_INACT1564', 'P14_POPMEN2554', 'NB_F119_NB_COU', 'C14_PMEN_MENFAM', 'DEC_TP6014', 'P14_POP30P', 'P14_PMEN', 'NB_D602', 'NB_D603', 'P14_NPER_RP_LOCHLMV', 'NB_D606', 'NB_D604', 'NB_D605', 'NB_F112_NB_AIREJEU', 'C102_NB_EP', 'P14_ANEM_RP_GRAT', 'C302_NB_INT', 'P14_NBPI_RP_ANEM0509', 'NB_F118_NB_AIREJEU', 'P14_HACT2554', 'P14_FNSAL15P', 'P14_POP1114', 'P14_NBPI_RP_ANEM10P', 'C14_H15P_CS6', 'C14_H15P_CS7', 'C14_H15P_CS4', 'C14_H15P_CS5', 'C14_H15P_CS2', 'C14_H15P_CS3', 'C14_H15P_CS1', 'C14_ACTOCC15P_MAR', 'DEC_Q314', 'P14_RPAPPART_ACH45', 'P14_FNSCOL15P_DIPLMIN', 'P14_H5564', 'DEC_D114', 'P14_POP2554_PSEUL', 'NB_F102_NB_AIREJEU', 'P14_RP_ACH70', 'P14_RPMAISON_ACH05', 'P14_RP_BDWC', 'C303_NB_INT', 'C105_NB_EP', 'note', 'P14_SCOL1517', 'P14_POP5564', 'P14_POP1564', 'C104_NB_EP', 'C14_ACT1564', 'P14_SAL15P_CDI', 'P14_NSAL15P_EMPLOY', 'P14_H1529', 'C105_NB_CANT', 'NB_F109', 'NB_F108', 'NB_F107', 'NB_F106', 'NB_F105', 'NB_F104', 'NB_F103', 'P14_HSAL15P_TP', 'P14_FACTOCC5564', 'P14_H1524', 'C14_POP15P_CS3', 'C605_NB_INT', 'C14_POP15P_CS1', 'C14_POP15P_CS7', 'C14_POP15P_CS6', 'C14_POP15P_CS5', 'C14_POP15P_CS4', 'C14_POP15P_CS8', 'P14_FSAL15P_TP', 'P14_HACT1524', 'C201_NB_INT', 'P14_SAL15P_CDD', 'P14_POP15P_PSEUL', 'P14_RPAPPART_ACH70', 'NB_F121_NB_COU', 'P14_HETUD1564', 'P14_NBPI_RP_ANEM0002', 'P14_RP_VOIT1', 'C14_ACTOCC1564_CS6', 'C14_ACTOCC1564_CS5', 'C14_ACTOCC1564_CS4', 'C14_ACTOCC1564_CS3', 'C14_ACTOCC1564_CS2', 'C14_ACTOCC1564_CS1', 'P14_PMEN_ANEM0002', 'P14_PMEN_ANEM0204', 'P14_H0014', 'P14_RP_VOIT2P', 'P14_H0019', 'LIBCOM', 'C304_NB_CANT', 'P14_FACTOCC15P', 'P14_HACT5564', 'C14_PMEN_MENSFAM', 'NB_F102_NB_COU', 'P14_RP_4P', 'NB_F113_NB_ECL', 'P14_FNSCOL15P', 'C14_H15P_CS8', 'P14_POP0305', 'NB_F118_NB_ECL', 'P14_NSAL15P_AIDFAM', 'NB_F103_NB_AIREJEU', 'P14_RP_TTEGOU', 'P14_POP2064', 'P14_MEN_ANEM0509', 'P14_NSAL15P_INDEP', 'NB_F112_NB_ECL', 'P14_POP', 'NB_F111_NB_ECL', 'NB_F117_NB_COU', 'C101_NB_CANT', 'NB_B316', 'NB_B315', 'NB_B312', 'NB_B313', 'NB_B310', 'NB_B311', 'P14_H4559', 'C509', 'P14_H65P', 'P14_POP0002', 'C501', 'C502', 'C503', 'C504', 'C505', 'P14_NSCOL15P_SUP', 'NB_D208', 'NB_D209', 'NB_D206', 'NB_D207', 'C14_MEN', 'NB_D205', 'NB_D202', 'NB_D203', 'NB_D201', 'C14_ACTOCC15P_PAS', 'C402', 'NB_A106', 'NB_A107', 'NB_A104', 'NB_A105', 'NB_D204', 'NB_A108', 'NB_A109', 'DEC_MED14', 'C14_COUPAENF', 'NB_F101_NB_AIREJEU', 'P14_NSCOL15P_DIPLMIN', 'NB_A101', 'NB_F110_NB_ECL', 'LIB_IRIS', 'C14_MENFSEUL', 'P14_NSCOL15P_BAC', 'NB_F117', 'P14_HNSCOL15P_CAPBEP', 'P14_POP1517', 'C603', 'C602', 'C601', 'C14_FAM', 'C604', 'C609', 'NB_F113', 'COM', 'C14_MENFAM', 'P14_RP_ACH90', 'P14_LOG', 'P14_POP5579_PSEUL', 'P14_POP6074', 'NB_F201_NB_AIREJEU', 'P14_RP_1P', 'P14_F75P', 'NB_F106_NB_COU', 'P14_NSAL15P', 'P14_NPER_RP_LOC', 'P14_RP_3P', 'P14_RPAPPART_ACH90', 'P14_HACT1564', 'NB_F203_NB_AIREJEU', 'NB_F116_NB_AIREJEU', 'P14_RP_4060M2', 'P14_ANEM_RP_LOC', 'C302_NB_PGE', 'NB_A506', 'NB_A507', 'NB_A504', 'NB_A505', 'NB_A502', 'NB_A503', 'NB_A501', 'C302_NB_EP', 'NB_F117_NB_ECL', 'P14_SAL15P_APPR', 'NB_D703', 'NB_D702', 'NB_D701', 'NB_F201', 'NB_D705', 'NB_D704', 'NB_D709', 'C14_MENSFAM', 'DEC_GI14', 'P14_FRETR1564', 'P14_RP_EAUCH', 'P14_H75P', 'C14_ACT1564_CS6', 'C505_NB_INT', 'LIB_COM', 'NB_F114_NB_ECL', 'P14_RPMAISON_ACH19', 'P14_POP2554', 'P14_FNSCOL15P_CAPBEP', 'P14_MEN_ANEM0204', 'C305_NB_CANT', 'P14_F3044', 'DEC_RD14', 'C301_NB_INT', 'NB_F113_NB_COU', 'P14_FNSCOL15P_BAC', 'P14_POP_IMM', 'NB_D401', 'NB_D402', 'NB_D403', 'NB_D404', 'NB_D405', 'NB_D237', 'NB_D236', 'NB_D235', 'C701', 'NB_D233', 'NB_D232', 'NB_D231', 'P14_SCOL30P', 'NB_D239', 'NB_D238', 'NB_B103', 'NB_B102', 'NB_B101', 'P14_RP_CLIM', 'DEC_PBEN14', 'P14_POP80P_PSEUL', 'C303_NB_PGE', 'P14_RP_ELEC', 'P14_ACTOCC1564', 'P14_POP4559', 'NB_F118', 'NB_F119', 'NB_F114', 'NB_F116', 'P14_RSECOCC', 'NB_F110', 'NB_F111', 'NB_F112', 'C14_PMEN_MENCOUPSENF', 'NB_D242', 'NB_D243', 'NB_D240', 'C14_MEN_CS4', 'C14_MENCOUPSENF', 'NB_F105_NB_AIREJEU', 'C14_MENPSEUL', 'P14_F65P', 'C14_PMEN_MENPSEUL', 'P14_H2064', 'P14_POP1117', 'P14_HACTOCC1564', 'C14_MEN_CS2', 'C14_COUPSENF', 'C101_NB_RPIC', 'P14_FACT2554', 'P14_LOGVAC', 'P14_PHORMEN', 'P14_RP_CCIND', 'C605', 'P14_F2064', 'P14_HINACT1564', 'NB_F107_NB_AIREJEU', 'P14_RP_80100M2', 'P14_APPART', 'P14_RPAPPART_ACH05', 'P14_NBPI_RP_ANEM0204', 'P14_FCHOM1564', 'P14_ACTOCC1524', 'P14_POP_FR', 'P14_POP75P', 'NB_F108_NB_AIREJEU', 'P14_NSCOL15P', 'P14_POP1524_PSEUL', 'P14_ACTOCC15P_ILT2', 'P14_ACTOCC15P_ILT1', 'P14_ACTOCC15P_ILT5', 'P14_ACTOCC15P_ILT4', 'NB_F120_NB_COU', 'NB_F305', 'NB_F304', 'C201_NB_CANT', 'NB_F303', 'NB_F302', 'P14_HACTOCC5564', 'P14_RP_PROP', 'P14_AINACT1564', 'NB_F109_NB_COU', 'P14_ACTOCC5564', 'P14_PMEN_ANEM0509', 'NB_F105_NB_COU', 'C104', 'C105', 'P14_H3044', 'DEC_D714', 'P14_SCOL0610', 'C101', 'C102', 'P14_FSAL15P', 'NB_F202_NB_AIREJEU', 'C302_NB_CANT', 'NB_D212', 'P14_SCOL1824', 'P14_SCOL0205', 'P14_POPF', 'P14_POP3044', 'P14_POP15P_MARIEE', 'P14_RP_ACH19', 'P14_POP2529', 'P14_POPH', 'LAB_IRIS', 'C201', 'C104_NB_RPIC', 'DEC_PCHO14', 'P14_POPMEN5579', 'P14_F0014', 'P14_HACTOCC1524', 'NB_F102_NB_ECL', 'C14_PMEN_MENFSEUL', 'C301_NB_CANT', 'P14_POP_ETR', 'P14_NPER_RP_PROP', 'P14_RPAPPART_ACHTOT', 'P14_SAL15P_INTERIM', 'P14_HNSCOL15P_DIPLMIN', 'P14_RP_LOCHLMV', 'P14_RP_VOIT1P', 'P14_ACTOCC15P_ILT2P', 'C101_NB_EP', 'NB_F112_NB_COU', 'C14_MENFAMMONO', 'P14_ANEM_RP_PROP', 'NB_F102', 'NB_F107_NB_ECL', 'P14_HSAL15P', 'NB_F101', 'C14_ACT1564_CS1', 'C14_ACT1564_CS3', 'C14_ACT1564_CS2', 'C14_ACT1564_CS5', 'C14_ACT1564_CS4', 'P14_RP_ACHTOT', 'DEC_D914', 'C14_POP15P_CS2', 'C702', 'NB_F108_NB_COU', 'C14_NE24F4P', 'P14_RP_6080M2', 'P14_CHOM1564', 'P14_HNSAL15P', 'NB_A301', 'NB_A302', 'NB_A303', 'NB_A304', 'NB_F203', 'P14_RP_SDB', 'C14_FAMMONO', 'P14_RP_GARL', 'P14_HACTOCC2554', 'NB_F105_NB_ECL', 'P14_RP_2P', 'P14_FACT1564', 'P14_F4559', 'P14_FAINACT1564', 'P14_RP_CINDELEC', 'C14_ACTOCC1564', 'NB_B201', 'NB_B202', 'NB_B203', 'NB_B204', 'NB_B205', 'NB_B206', 'DEC_D614', 'NB_F103_NB_COU', 'P14_RPMAISON_ACH11', 'P14_HNSCOL15P_SUP', ]


def update_list_indicators():
    pass
    '''
    from __future__ import print_function  # to be put at beginning of imports
    import random
    all_iris_69 = json_utils.parse_json_to_dict(geojson_integrated_output_file_69)
    random_start_iris = random.choice(all_iris_69["features"])  # one random iris
    list_features = random_start_iris["properties"][geojson_grouped_indicators_label].keys()
    for item in list_features:
        print("'" + item + "', ", end="")
    list_features2 = random_start_iris["properties"][geojson_indicators_label].keys()
    for item in list_features2:
        print("'" + item + "', ", end="")
    '''

