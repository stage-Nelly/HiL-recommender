#!/usr/bin/env python
# encoding: utf-8 
# =============================================================================
# Recommender: main class for recommender algorithms
# =============================================================================

from vizliris import config
import sklearn
import numpy as np
import statistics
import heapq  # for top-k results in dict
from sklearn.metrics.pairwise import cosine_similarity
from sklearn import cluster
import hdbscan

print("scikit-learn version " + sklearn.__version__)

unknown_value = 0


def build_feature_vectors(list_iris, list_features, use_neighborhood=False):
    """
    Builds the feature vectors (and a list of corresponding iris codes) for a given list of iris and features
    :param list_iris: list of iris from which to build a list of feature vectors
    :param list_features: the features to be used for recommending
    :return: vectors_iris: a list of list of features (each internal list is the vector of one iris)
    :return: codes_iris: a list of iris codes, each corresponding to its vector in vectors_iris
    """
    vectors_iris = list()  # list of vectors (lists) for iris features
    codes_iris = list()  # codes of iris (corresponding to each vector)
    for iris in list_iris:
        code_i = iris["properties"]["CODE_IRIS"]
        vector_i = list()
        for f in list_features:
            # feature in raw indicators (config.geojson_indicators_label)
            if config.geojson_indicators_label in iris["properties"] and f in iris["properties"][
               config.geojson_indicators_label]:
                vector_i.append(iris["properties"][config.geojson_indicators_label][f])
            # feature in grouped indicators (config.geojson_grouped_indicators_label) and in direct neighbor grouped indicators
            elif config.geojson_grouped_indicators_label in iris["properties"] and f in iris["properties"][
                config.geojson_grouped_indicators_label]:
                vector_i.append(iris["properties"][config.geojson_grouped_indicators_label][f])
                if use_neighborhood:
                    vector_i.append(iris["properties"][config.geojson_direct_neighbourhood_grouped_indicators_label][f])
            else:  # feature not found
                vector_i.append(unknown_value)  # -1 or 0 or ??
        vectors_iris.append(vector_i)
        codes_iris.append(code_i)
    return vectors_iris, codes_iris


def build_temp_iris(code_iris, list_features, list_values):
    # build a fake temp iris (from aggregated data for instance, eg, with svm) for building a vector for ML algos
    iris = dict()
    iris["properties"] = dict()
    iris["properties"]["CODE_IRIS"] = code_iris
    iris["properties"][config.geojson_grouped_indicators_label] = dict()
    iris["properties"][config.geojson_direct_neighbourhood_grouped_indicators_label] = dict()
    for i in range(0, len(
            list_features)):  # for two successive values, one is for the iris and one for its direct neighbors
        feature = list_features[i]
        value = list_values[i]
        # value2 = list_values[i+1]
        iris["properties"][config.geojson_grouped_indicators_label][feature] = value
    # iris["properties"][config.geojson_direct_neighbourhood_grouped_indicators_label][feature] = value2
    return iris


def compute_justifications_cosine(vector_start_iris, vector_candidate_iris, list_features, nb_justifications=3):
    """
    Produce the justification for recommending the candidate iris according to start iris with cosine. Use the vectors
    of both iris to derive the nb_justifications most important features (i.e., labels selected in list_features).
    :param vector_start_iris:
    :param candidate_vector_iris:
    :param list_features:
    :return:
    """
    temp = [a * b for a, b in zip(vector_start_iris, vector_candidate_iris)]  # multiply i^th elements (i.e., cosine)
    temp = sorted(range(len(temp)), key=lambda k: temp[k])  # sort the list (ascending) and store indexes
    temp = temp[len(temp) - nb_justifications:]
    justifications = [list(list_features)[i] for i in temp]
    # print justifications
    return justifications


def recommend_cosine_measure(start_iris, candidate_iris, list_features, topk=10):
    """
    Performs simple recommendations (cosine measure) between one start iris and a list of candidate iris.
    :param start_iris: a starting iris (e.g., where the person comes from) used for prediction/recommendation
    :param candidate_iris: a list of iris representing the area where the person is going to live
    :param list_features: the features to be used for recommending
    :param topk: the number of recommendations to return (default to 20)
    :return: recommendations: a dict of topk recommended iris with their similarity score
    """
    vector_start_iris, _ = build_feature_vectors([start_iris], list_features)
    vectors_candidate_iris, codes_candidate_iris = build_feature_vectors(candidate_iris, list_features)
    recommendations = dict()
    justifications = dict()

    for count in range(0, len(codes_candidate_iris)):  # apply cosine measure between start_vector and each candidate
        vector_iris = vectors_candidate_iris[count]
        code_iris = codes_candidate_iris[count]
        sim = cosine_similarity(np.array([vector_start_iris[0]]), np.array([vector_iris]))
        recommendations[code_iris] = round(sim[0][0],
                                           4)  # cosine returns an array of list of list such as array([[sim]])
        # justifications
        #justifications[code_iris] = compute_justifications_cosine(vector_start_iris[0], vector_iris, list_features)

    print("rec" + str(recommendations))

    topk_recommendations = heapq.nlargest(topk, recommendations, key=recommendations.__getitem__)
    # print("topk recommendations : ")
    # print(topk_recommendations)
    all_recommendations = dict(recommendations)
    for reco in all_recommendations:
        if reco not in topk_recommendations:
            del recommendations[reco]  # delete unrecommended iris
            #del justifications[reco]  # delete justifications of unrecommended iris
    print("recommendations : " + str(recommendations))
    # print("justifications : " + str(justifications))
    return recommendations, justifications


def recommend_standard_deviation(start_iris, end_iris, list_features, topk=10):
    """
    Performs recommendation between a list of start iris and a list of candidate (end) iris.
    The idea is to build a profile vector of the most important indicators from the start iris.
    For each indicator, the standard deviation is computed: values close to 0 means the indicator shares very close
    values whatever the iris, and higher values could indicate that the indicator is less important.
    The profile vector is built from the average values of an indicator weighted by standard deviation. It has the value
    of the average if std=0 (all value identical), else the average is lowered when multiplied by the std weight.
    The profile vector is then compared (using cosine measure) to each candidate iris.
    :param start_iris: a list of starting iris (e.g., where the person comes from) used for prediction/recommendation
    :param end_iris: a list of iris representing the area where the person is going to live
    :param list_features: the features to be used for recommending
    :param topk: the number of recommendations to return (default to 20)
    :return: sorted_recommendations: a dict of topk recommended iris with their similarity score
    """
    if isinstance(start_iris, dict):  # if only one IRIS
        vectors_start, codes_start = build_feature_vectors([start_iris], list_features)
    else:  # if several IRIS
        vectors_start, codes_start = build_feature_vectors(start_iris, list_features)
    vectors_end, codes_end = build_feature_vectors(end_iris, list_features)

    weight_vector = []  # the vector which contains the standard deviation for each indicator
    avg_vector = []  # the vector which contains the average/mean for each indicator
    w_start, h_start = len(vectors_start[0]), len(vectors_start)  # width and height of starts_iris

    for col in range(0, w_start):  # for each indicator
        column = []  # a column for computing the std of the indicator
        for row in range(0, h_start):  # for each iris
            column.append(vectors_start[row][col])  # append the value of the indicator for a given iris
        deviation = statistics.pstdev(column)  # calculate the deviation for an indicator (anf for all start iris)
        average = statistics.harmonic_mean(column)  # calculate the mean for an indicator (anf for all start iris)
        if deviation <= 0.0001:  # deviation=0 ou deviation ~= 0 (ie, values are very similar for this indicator), avoid divide by 0 error
            weight = 1.0
        else:  # deviation >> 0 (ie, values can be very different -or not- for this indicator)
            weight = 1.0 / deviation
        weight_vector.append(weight)
        avg_vector.append(average)
    print("weight vector = " + str(weight_vector))
    print("avg vector = " + str(avg_vector))

    assert(len(weight_vector) == len(vectors_start[0])), "Error in std recommendation: the weight vector should contain" \
                                                          "the same number of indicators than in initial IRIS vectors."
    assert(len(weight_vector) == len(avg_vector)), "Error in std recommendation: the weight vector should contain" \
                                                          "the same number of values than the avg vector."

    # building the profile vector (with average values of start iris but leveraged by the weight of std)
    profile_vector = list()
    len_weight_vector = len(weight_vector)
    for index in range(0, len_weight_vector):
        #TODO: maybe not the best solution to multiply (values are quite low)
        profile_vector.append(avg_vector[index] * weight_vector[index])
    print("profile vector = " + str(profile_vector))

    # compare the profile vector to each candidate using cosine measure, and keep top-k recommendations
    recommendations = dict()
    for count in range(0, len(codes_end)):  # apply cosine measure between start_vector and each iris in end_iris
        vector_iris = vectors_end[count]
        code_iris = codes_end[count]
        sim = cosine_similarity(np.array([profile_vector]), np.array([vector_iris]))
        recommendations[code_iris] = round(sim[0][0],
                                           4)  # cosine returns an array of list of list such as array([[sim]])
    topk_recommendations = heapq.nlargest(topk, recommendations, key=recommendations.__getitem__)
    all_recommendations = dict(recommendations)
    for reco in all_recommendations:
        if reco not in topk_recommendations:
            del recommendations[reco]  # delete unrecommended iris
    print("recommendations = " + str(recommendations))
    return recommendations


def recommend_clustering(candidate_iris, list_features, start_iris=None, algorithm='kmeans', topk=10,
                         nb_justifications=3):
    """
    Performs clustering based on different algorithms from scikit-learn. If start_iris provided, also
    recommend the best cluster (and so its iris) for this iris_prediction.
    For justifications,clustering algorithms are a black-box, difficult to extract internal data. Heuristic : we have
    the max/highest values of each feature, the idea is to find the cluster for each feature. We generate one vector
    for each feature (all values at 0.0 except the highest value for the current feature) and predict the cluster
    for this vector / feature.
    :param candidate_iris: a list of iris representing the area where the person is going to live
    :param list_features: the features to be used for recommending
    :param start_iris: a starting iris (e.g., where the person comes from) used for prediction/recommendation
    :param algorithm: name of the algorithm (default to 'kmeans')
    :param topk: number of expected recommended iris - used for computing number of clusters
    :param nb_justifications : number of justifications (features) provided to justify a recommended iris
    :return: a dict of recommandations, a number of clusters
    """
    justifications = dict()
    vector_max_values = [0.0] * len(list_features)  # for justification, store the highest values for each feature

    vectors_candidate_iris, codes_candidate_iris = build_feature_vectors(candidate_iris, list_features)
    for vector in vectors_candidate_iris:  # for each candidate, check and store the highest value
        vector_max_values = [max(a, b) for a, b in zip(vector, vector_max_values)]

    cluster_id_predicted = None

    recommendations = dict()
    # expecting topk iris, so nb_clusters = nb_iris / topk (+ 1 in case nb_iris < top-K)
    nb_clusters = int(1 + (len(codes_candidate_iris) / topk))

    # clustering - many parameters available for each clustering algorithm
    if algorithm == 'affinity':
        clusters = cluster.AffinityPropagation().fit(np.array(vectors_candidate_iris))
    elif algorithm == 'agglo':
        clusters = cluster.AgglomerativeClustering(n_clusters=nb_clusters).fit(np.array(vectors_candidate_iris))
    elif algorithm == 'birch':
        clusters = cluster.Birch(n_clusters=nb_clusters).fit(np.array(vectors_candidate_iris))
    elif algorithm == 'dbscan':
        clusters = cluster.DBSCAN().fit(np.array(vectors_candidate_iris))
    elif algorithm == 'featureagglo':
        clusters = cluster.FeatureAgglomeration(n_clusters=nb_clusters).fit(np.array(vectors_candidate_iris))
    elif algorithm == 'meanshift':
        clusters = cluster.MeanShift().fit(np.array(vectors_candidate_iris))
    elif algorithm == 'minimbatchkmeans':
        clusters = cluster.MiniBatchKMeans(n_clusters=nb_clusters).fit(np.array(vectors_candidate_iris))
    elif algorithm == 'spectral':
        clusters = cluster.SpectralClustering(n_clusters=nb_clusters).fit(np.array(vectors_candidate_iris))
    else:  # kmeans by default
        clusters = cluster.KMeans(n_clusters=nb_clusters, random_state=0).fit(np.array(vectors_candidate_iris))
    # print clusters.labels_

    if hasattr(clusters, "predict"):
        # build the vector for start_iris to perform a prediction / recommendation of the best cluster
        if start_iris is not None:
            vector_start_iris, _ = build_feature_vectors([start_iris], list_features)
            cluster_id_predicted = clusters.predict(vector_start_iris)[0]
        # print cluster_id_predicted
    else:
        start_iris = None  # just in case, to avoid an exception

    nb_distinct_clusters = len(set(clusters.labels_))  # nb clusters
    for count in range(0, len(codes_candidate_iris)):
        code_iris = codes_candidate_iris[count]
        if start_iris is not None:  # iris recommended for code_iris_prediction
            if clusters.labels_[count] == cluster_id_predicted:
                recommendations[code_iris] = int(clusters.labels_[count])
        else:  # all iris added to recommandations
            recommendations[code_iris] = int(clusters.labels_[count])

    # justifications - currently uses clusters.predict(), which is not available for all algorithms
    """
    if hasattr(clusters, "predict"):  # justifications only for algorithms with predict() function
        # for justifications, computing the important feature(s) of each cluster
        cluster_to_features = dict()  # {1: ["education", "services"], 2: ["commerces"], ...}
        for i in range(0, len(vector_max_values)):
            # build a temp vector for the feature
            temp_vector = [0.0] * len(list_features)
            current_feature = list_features[i]
            temp_vector[i] = vector_max_values[i]  # set the highest value for current feature
            cluster_id = clusters.predict([temp_vector])[0]  # cluster_id predicted for current_feature
            if cluster_id not in cluster_to_features:
                cluster_to_features[cluster_id] = list()
            cluster_to_features[cluster_id].append(current_feature)
        # print cluster_to_features
        # computing justification for each recommended iris using cluster_to_features
        for code_iris, cluster_id in recommendations.items():
            if cluster_id not in cluster_to_features:  # this cluster was not associated to any feature
                justifications[code_iris] = None
            elif len(cluster_to_features[cluster_id]) < nb_justifications:  # not enough features for this cluster
                justifications[code_iris] = list(cluster_to_features[cluster_id])
            else:  # too many features, need to select the most appropriate ones for the given iris
                justifications[code_iris] = list()
                index_features = list()
                for feature in cluster_to_features[cluster_id]:  # get the features index
                    index_features.append(list_features.index(feature))
                current_vector = vectors_candidate_iris[
                    codes_candidate_iris.index(code_iris)]  # get vector of rec. iris
                sorted_indexes = sorted(range(len(current_vector)), key=lambda k: current_vector[k],
                                        reverse=True)  # sort the indexes (descending)
                for index in sorted_indexes:
                    if index in index_features:  # this index feature is relevant for the cluster
                        if len(justifications[code_iris]) < nb_justifications:  # add the feature for the rec. iris
                            justifications[code_iris].append(list_features[index])
        # print justifications
    """
    return recommendations, nb_distinct_clusters, justifications


def recommend_svm(start_iris, candidate_iris, list_features, algorithm='linear_svc'):
    """
    Perform content-based recommendation based on SVM machine learning algorithms. From the different start iris,
    an SVM algorithm builds a profile of weights/coefficients for each indicator. This profile is then applied to each
    candidate iris and the one(s) with the highest score are recommended.
    # http://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html
    :param start_iris: a list of starting iris (e.g., where the person comes from) used for prediction/recommendation
    :param candidate_iris: a list of iris representing the area where the person is going to live
    :param list_features: the features to be used for recommending
    :param algorithm: name of the algorithm (default to 'linear_svc')
    :return:
    """
    vectors_start_iris, codes_start_iris = build_feature_vectors([start_iris], list_features)
    vectors_candidate_iris, codes_candidate_iris = build_feature_vectors(candidate_iris, list_features)
    recommendations = dict()

    classes = list()  # used for training - all start iris have class 1
    for i in range(0, len(vectors_start_iris)):
        classes.append(1)

    if algorithm == 'one_class_svm':  # specific svm algorithm because only one class
        clf = sklearn.svm.OneClassSVM(kernel='linear')
        vectors_start = vectors_start_iris

    else:  # multi-class svm algorithms (class 0 for neigbors which are not selected and class 1 for all start iris)
        pass  # multiclass SVM not used because not totally working - needs to review this code
        """
        neighbour_start_iris = list()  # list of not selected iris that are neighbours to any start_iris
        for iris in start_iris:  # finding the neighbor iris that are not in start_iris and that will have class 0
            for iris_code in iris["properties"][config.geojson_direct_neighbourhood_label]:
                if iris_code not in codes_start_iris:  # add as neighbour iris
                    neighbour_iris = geo_utils.get_iris_from_code_iris(iris_code)
                    neighbour_start_iris.append(neighbour_iris)  # getting neighbor iris from their codes
        vectors_neighbour_start_iris, _ = build_feature_vectors(neighbour_start_iris, list_features)
        vectors_start = vectors_start_iris + vectors_neighbour_start_iris
        for i in range(0, len(vectors_neighbour_start_iris)):
            classes.append(0)
        if algorithm == 'nu_svc':
            clf = sklearn.svm.NuSVC(kernel='linear')
        elif algorithm == 'nu_svr':
            clf = sklearn.svm.NuSVR(kernel='linear')
        elif algorithm == 'svr':
            clf = sklearn.svm.SVR(kernel='linear')
        else:
            clf = sklearn.svm.LinearSVC()
        """

    clf.fit(vectors_start, classes)  # training
    # todo : cosine ? clustering ? manual assignment of coef/weights to candidate iris ?
    # print(clf.coef_[0])  # coefficients correspond to weights for each indicator, i.e., the profile
    # use the weighted vector as iris_start and do clustering
    # weight_iris = build_temp_iris("weight-iris", list_features, clf.coef_[0])
    # return recommend_clustering(candidate_iris, list_features, weight_iris)

    predicted_classes = clf.predict(vectors_candidate_iris)
    for count in range(0, len(codes_candidate_iris)):
        code_iris = codes_candidate_iris[count]
        predicted_class = int(predicted_classes[count])
        if predicted_class == 1:  # class 1 is class of pertinents IRIS
            recommendations[code_iris] = predicted_class
    # print(recommendations)
    return recommendations


def recommend_hdbscan(candidate_iris, list_features, start_iris=None, min_cluster_size=5, min_samples=5):
    """
    Performs clustering based on the HDBSCAN algorithm.
    :param candidate_iris: a list of iris representing the area where the person is going to live
    :param list_features: the features to be used for recommending
    :param start_iris: a starting iris (e.g., where the person comes from) used for prediction/recommendation
    :param min_cluster_size: HDBSCAN parameter, minimum number of iris per cluster
    :param min_samples : HDBSCAN parameter, a larger value means more points will be declared as noise
    :return: a dict of recommendations, a number of clusters
    """
    recommendations = dict()
    vectors_candidate_iris, codes_candidate_iris = build_feature_vectors(candidate_iris, list_features)
    cluster_id_predicted = None

    # verifying the validity of parameters
    if len(candidate_iris) > min_samples:
        min_samples = len(candidate_iris)

    # if start_iris, need to predict the cluster of start_iris (recommendation), requires option 'prediction_data=True'
    if start_iris is not None:
        vector_start_iris, _ = build_feature_vectors([start_iris], list_features)
        clusterer = hdbscan.HDBSCAN(min_cluster_size=min_cluster_size, min_samples=min_samples, prediction_data=True)
        clusterer.fit(np.array(vectors_candidate_iris))
        cluster_id_predicted = hdbscan.approximate_predict(clusterer, vector_start_iris)[0]
    else:  # clustering an area, no prediction
        clusterer = hdbscan.HDBSCAN(min_cluster_size=min_cluster_size, min_samples=min_samples,
                                    cluster_selection_method='leaf')  # for not having a big cluster
        clusterer.fit(np.array(vectors_candidate_iris))

    # computing recommendations
    nb_distinct_clusters = len(set(clusterer.labels_))-1  # nb clusters, -1 because noise is labelled "-1"
    for count in range(0, len(codes_candidate_iris)):
        code_iris = codes_candidate_iris[count]
        if start_iris is not None:  # iris recommended for code_iris_prediction
            if clusterer.labels_[count] == cluster_id_predicted:
                recommendations[code_iris] = int(clusterer.labels_[count])
        else:  # all iris added to recommandations
            recommendations[code_iris] = int(clusterer.labels_[count])

    # clusterer.probabilities_  gives the confidence of each point to be in the cluster
    # clusterer.cluster_persistence_ returns a score of 1.0 for stable clusters down to 0.0 for ephemeral clusters
    return  recommendations, nb_distinct_clusters



