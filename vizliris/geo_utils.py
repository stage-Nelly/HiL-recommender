#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# geo_utils contains spatial utilities (e.g., point in polygon)
# shapely doc: http://toblerity.org/shapely/manual.html
# geopy doc: https://geopy.readthedocs.io/en/latest/
# Requires the installation of gdal (http://www.gdal.org/)
#   To install GDAL on Ubuntu, see https://trac.osgeo.org/ubuntugis/wiki/UbuntuGISRepository
# =============================================================================

from vizliris import config
from vizliris import json_utils
from vizliris import regroupement_indicateurs
from mongiris import Mongiris

db = Mongiris()
db.init_connection()


#TODO
#TODO check if these functions are still needed (most have been deleted)
#TODO actually the distance between 2 points (iris) is not implemented in Mongo, so should have kept it...
#TODO


def get_grouped_indicators_direct_neighbors(list_iris):
    # get grouped indicators of direct neighbours for a list of iris and returns an updated list of iris
    neighborhoods = json_utils.parse_json_to_dict(config.json_neighborhoods_FR)
    updated_list_iris = list(list_iris)
    for iris in updated_list_iris:
        code_iris = iris["properties"]["CODE_IRIS"]
        if code_iris in neighborhoods and config.geojson_direct_neighbourhood_grouped_indicators_label in neighborhoods[
            code_iris]:
            iris["properties"][config.geojson_direct_neighbourhood_grouped_indicators_label] = neighborhoods[code_iris][
                config.geojson_direct_neighbourhood_grouped_indicators_label]
    return updated_list_iris


def compute_grouped_indicators_direct_neighbors(list_codes_iris):
    # computes the grouped indicators of all iris in list_iris and updates the neighborhoods file
    neighborhoods = json_utils.parse_json_to_dict(config.json_neighborhoods_FR)
    dict_grouping_indicators = json_utils.parse_json_to_dict(
        config.grouping_indicators_file)  # how to group raw indicators
    for code_iris in list_codes_iris:  # for each iris, get its list of direct neighbors and compute grouped indicators
        if config.geojson_direct_neighbourhood_grouped_indicators_label not in neighborhoods[code_iris]:
            list_neighbors = neighborhoods[code_iris][config.geojson_direct_neighbourhood_label]
            temp_iris = dict()  # need to create a temp iris for computing grouped indicators
            temp_iris["properties"] = dict()
            temp_iris["properties"]["CODE_IRIS"] = "temp iris"
            temp_iris["properties"][config.geojson_indicators_label] = dict()
            for code_neighbor in list_neighbors:  # sum the raw indicators of each neighbor in a temp iris
                neighbor = db.get_iris_from_code(code_neighbor)
                if neighbor is not None and config.geojson_indicators_label in neighbor[
                    "properties"]:  # sum all its indicators
                    for ind in neighbor["properties"][config.geojson_indicators_label]:
                        if config.is_number(
                                neighbor["properties"][config.geojson_indicators_label][ind]):  # only keep numbers
                            if ind not in temp_iris["properties"][config.geojson_indicators_label]:  # no value, use this one
                                temp_iris["properties"][config.geojson_indicators_label][ind] = \
                                neighbor["properties"][config.geojson_indicators_label][ind]
                            else:  # sum values
                                sum = float(temp_iris["properties"][config.geojson_indicators_label][ind]) + float(
                                    neighbor["properties"][config.geojson_indicators_label][ind])
                                temp_iris["properties"][config.geojson_indicators_label][ind] = sum
            # compute the grouped indicators values for the temps iris
            grouped_indicators = regroupement_indicateurs.compute_grouped_indicators(temp_iris["properties"]
                                                                                     [config.geojson_indicators_label],
                                                                                     dict_grouping_indicators)
            neighborhoods[code_iris][config.geojson_direct_neighbourhood_grouped_indicators_label] = grouped_indicators
    json_utils.save_dict_to_json(config.json_neighborhoods_FR, neighborhoods)


def compute_profiles_ideal_neighbors_work_to_arrival():
    # compute and store the HiL profiles ideal neighbors, neighbor zone = distance(iris_work, iris_arrival) + 1 ou 2km
    # profile 000020 - iris 594370000 [ERROR] - shapely.geos - TopologyException: Input geom 1 is invalid: Self-intersection at or near point 2.1244298860372726 50.971212209886566 at 2.1244298860372726 50
    profiles_hil = json_utils.parse_json_to_dict(config.json_HiL_profils_from_to)
    neighborhoods = json_utils.parse_json_to_dict(config.json_neighborhoods_FR)

    for profile_id, profile in profiles_hil.items():
        code_iris_work = profile["code_iris_work"]
        code_iris_arrival = profile["code_iris_arrival"]
        iris_work = db.get_iris_from_code(code_iris_work)
        iris_arrival = db.get_iris_from_code(code_iris_arrival)
        if iris_work is not None and iris_arrival is not None:  # compute ideal neighbors
            distance_arrival_work = compute_min_distance_between_iris(iris_arrival["geometry"], iris_work["geometry"])
            dynamic_distance_1km = distance_arrival_work + 1.0
            dynamic_distance_2km = distance_arrival_work + 2.0
            try:
                iris_candidates_1km = build_distant_neighborhood(code_iris_work, dynamic_distance_1km)
                iris_candidates_2km = build_distant_neighborhood(code_iris_work, dynamic_distance_2km)
                neighbors = list()
                for iris in iris_candidates_1km:  # iris_candidates is a list of iris (with all information)
                    neighbors.append(iris["properties"]["CODE_IRIS"])
                if code_iris_arrival not in neighbors:  # many cases where the arrival iris is not included, so add it
                    neighbors.append(code_iris_arrival)
                neighborhoods[code_iris_work][config.geojson_profiles_ideal_neighbourhood_1km_label] = neighbors
                neighbors = list()
                for iris in iris_candidates_2km:  # iris_candidates is a list of iris (with all information)
                    neighbors.append(iris["properties"]["CODE_IRIS"])
                if code_iris_arrival not in neighbors:  # many cases where the arrival iris is not included, so add it
                    neighbors.append(code_iris_arrival)
                neighborhoods[code_iris_work][config.geojson_profiles_ideal_neighbourhood_2km_label] = neighbors
                print("OK for IRIS " + str(code_iris_work))
            except ValueError:
                print("Error for IRIS " + str(code_iris_work))
    json_utils.save_dict_to_json(config.json_neighborhoods_FR, neighborhoods)

