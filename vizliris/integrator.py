#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# Integrator: perform the integration of different data sources to produce
# a GeoJSON file containing IRIS information (for a specific area),
# INSEE indicators, sociology indicators, etc.
# =============================================================================

import os
from vizliris import config
from vizliris import json_utils
from vizliris import xls_utils
from vizliris import regroupement_indicateurs

#TODO this function needs to be rewritten to use MongoDB for storage instead of JSON files

# le fichier INSEE logements-2014.xlsx a été modifié (4 derniers colonnes uniquement remplies pour les DOM-TOM, donc
# erreur pour tous les iris non DOM-TOM)

'''
- les IRIS d'habitat (code H) : leur population se situe en général entre 1 800 et 5 000 habitants. Ils sont homogènes quant au type d'habitat et leurs limites s'appuient sur les grandes coupures du tissu urbain (voies principales, voies ferrées, cours d'eau, ...) ;
- les IRIS d'activité (code A) : ils regroupent environ 1 000 salariés et comptent au moins deux fois plus d'emplois salariés que de population résidente ;
- les IRIS divers (code D) : il s'agit de grandes zones spécifiques peu habitées et ayant une superficie importante (parcs de loisirs, zones portuaires, forêts, ...).
- les communes non découpées en IRIS, le type de l'IRIS est codé à Z
'''


def build_dictionary_indicators(dict_indicators, new_short_fieldnames, new_long_fieldnames, from_insee_file):
    """
    Adds new indicators (code, label and source file) in the dictionary dict_variables
    :param dict_indicators: a dict containing information about relevant indicators
    :param new_short_fieldnames: indicators codes to be added
    :param new_long_fieldnames: indicators full labels to be added
    :param from_insee_file: filepath of the INSEE file in which new indicators are extracted
    :return: res_dict_indicators: an updated version of dict_variables
    {ind1: {label: indicator1, insee_files=[file1, file2], ...}, ind2: {...}, ...}
    """
    res_dict_indicators = dict(dict_indicators)
    for i in range(0, len(new_short_fieldnames)):
        shortname = new_short_fieldnames[i]
        if shortname not in res_dict_indicators:
            res_dict_indicators[shortname] = dict()
            # res_dict_indicators[shortname]["short_fieldname"] = shortname
            res_dict_indicators[shortname][config.geojson_longname_label] = new_long_fieldnames[i]
        if config.geojson_insee_files_label not in res_dict_indicators[shortname]:
            res_dict_indicators[shortname][config.geojson_insee_files_label] = list()
        if from_insee_file not in res_dict_indicators[shortname][config.geojson_insee_files_label]:
            res_dict_indicators[shortname][config.geojson_insee_files_label].append(from_insee_file)
    return res_dict_indicators


def integrate_xls_file(iris_dict, indicators_xls):
    """
    Integrate IRIS data with indicators (about IRIS) and produce a dict of IRIS.
    Careful : indicators are not available for all IRIS, and some indicators concern IRIS #69029ZZZZ (ZZZZ meaning the
    neighbourhood, not the IRIS).
    :param iris_dict: a dictionary with IRIS data (geojson format)
    :param indicators_xls: a csv file path containing INSEE indicators
    :return: short_fieldnames: a list containing field ID (or abbreviated field names)
    :return: long_fieldnames: a list containing the complete field names
    :return: res_iris_dict: a geojson merged dict that integrates both IRIS data and INSEE indicators
    """
    short_fieldnames, long_fieldnames, indicators = xls_utils.parse_xls_to_dict(indicators_xls)
    ''' # only store relevant indicators (those in config.indicators_ids)
    sf = list(short_fieldnames)  # need to create a temp list
    for field in sf:  
        if field not in config.indicators_ids:  # todo : delete if we store all indicators
            index_field = short_fieldnames.index(field)
            del short_fieldnames[index_field]
            del long_fieldnames[index_field]
            for key in indicators.keys():
                del indicators[key][field]
    '''

    res_iris_dict = dict(iris_dict)
    for key, prop_values in indicators.items():
        for feature in res_iris_dict["features"]:
            if key == feature["properties"]["CODE_IRIS"]:  # indicator record concerns an iris
                if config.geojson_indicators_label not in feature["properties"]:
                    feature["properties"][config.geojson_indicators_label] = dict()
                for prop, value in prop_values.items():
                    if prop not in feature["properties"] and prop in config.metadata_about_iris: # adding a metadata
                        feature["properties"][prop] = value
                    if prop not in feature["properties"][config.geojson_indicators_label] and prop not in feature["properties"]:
                        feature["properties"][config.geojson_indicators_label][prop] = value
                break
    return short_fieldnames, long_fieldnames, res_iris_dict


def build_store_index(input_iris_dict, index_output_filepath):
    """
    Create an index for iris and store it in a JSON file.s
    :param input_iris_dict: a dictionary with IRIS data (geojson format)
    :param index_output_filepath: a filepath to the json file in which the index is stored
    :return:
    """
    index = dict()
    nb_iris = len(input_iris_dict["features"])
    for i in range(0, nb_iris):
        code_iris = input_iris_dict["features"][i]["properties"]["CODE_IRIS"]
        index[code_iris] = i
    json_utils.save_dict_to_json(index_output_filepath, index)  # store index


def integrate_from_to(input_iris_dict, dict_indicators, iris_indicators_output_file):
    """
    Main integration program, integrates XLS data from a geojson dict and store the result as a geojson with indicators.
    All iris are (possibly) enriched with raw and grouped indicators.
    :param input_iris_dict: a dictionary with IRIS data (geojson format)
    :param dict_indicators: a dictionary with information about indicators (shortname, longname, etc.)
    :param iris_indicators_output_file: output filename for integrated geojson file
    :return: nothing :(
    """

    config.logger.info("Integrating raw indicators")
    for f in config.indicators_files:  # integrate each xlsx INSEE file (both indicators in IRIS and indicators dict)
        config.logger.info("Integration of xls INSEE file: " + f)
        short_fields, long_fields, input_iris_dict = integrate_xls_file(input_iris_dict, f)
        dict_indicators = build_dictionary_indicators(dict_indicators, short_fields, long_fields, f)

    # all xlsx files have been integrated, computing grouped indicators
    config.logger.info("Computing grouped indicators")
    dict_grouping_indicators = json_utils.parse_json_to_dict(config.grouping_indicators_file)  # how to group raw indicators
    for iris in input_iris_dict["features"]:
        if config.geojson_indicators_label in iris["properties"]:
            grouped_indicators = regroupement_indicateurs.compute_grouped_indicators(iris["properties"]
                                                            [config.geojson_indicators_label], dict_grouping_indicators)
            iris["properties"][config.geojson_grouped_indicators_label] = grouped_indicators

    config.logger.info("Storing output file: " + iris_indicators_output_file)
    json_utils.save_dict_to_json(iris_indicators_output_file, input_iris_dict)  # store enriched IRIS
    # the dictionary has the same name, except it ends with "-dictionnaire.json" instead of "geojson"
    dict_output_file = os.path.splitext(iris_indicators_output_file)[0] + "-dictionnaire.json"
    config.logger.info("Storing output file: " + dict_output_file)
    json_utils.save_dict_to_json(dict_output_file, dict_indicators)  # store dict indicators
    index_output_file = os.path.splitext(iris_indicators_output_file)[0] + "-index.json"
    config.logger.info("Building and storing output file: " + index_output_file)
    build_store_index(input_iris_dict, index_output_file)  # create and store an index file (code_iris to iris)


def integrate_by_department(input_iris_dict, dict_indicators):
    """
    Perform integration for each department (one geojson file per department).
    :param input_iris_dict: a dict of departments, each containing its IRIS as features in the GeoJSON format
    :param dict_indicators: a dictionary with information about indicators (shortname, longname, etc.)
    :return: nothing
    """
    # version non optimisee, les fichiers xls sont lus pour chaque departement (dans integrate_from_to)...
    for dep, iris_dep in input_iris_dict.items():
        #if dep in ["10", "11", "12", "13", "14", "15"]: # delete, only for testing
        iris_output_file = os.path.join(config.geojson_integrated_output_departement_dir, dep + ".geojson")
        print("========================\n Processing " + iris_output_file + "\n========================")
        if not os.path.exists(iris_output_file):
            integrate_from_to(iris_dep, dict_indicators, iris_output_file)


#######################
# starting integration
#######################
config.logger.info("Reading dictionary files")
dict_indicators = dict(config.indicators_dictionary_init)  # the initial dictionary (labels of main indicators)
config.logger.info("Reading IRIS geojson file")

# todo: iris69 should be deleted (config.geojson_integrated_output_file_69)
#iris_69 = select_iris_69(config.geojson_all_iris_file_path)
#integrate_from_to(iris_69, dict_indicators, config.geojson_integrated_output_file_69)

#iris_FR = json_utils.parse_json_to_dict(config.geojson_all_iris_file_path)
#integrate_from_to(iris_FR, dict_indicators, config.geojson_integrated_output_file_FR)

#iris_by_department = json_utils.extract_iris_by_department(config.geojson_all_iris_file_path)
#integrate_by_department(iris_by_department, dict_indicators)


#######################
# end integration
#######################
config.logger.info("Done !")
