#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
#   JSON utilities (parsing, storing, selection of IRIS, etc.)
# =============================================================================

import json
from vizliris import xls_utils
from vizliris import config
from mongiris import Mongiris

def parse_json_to_dict(json_file_path):
    with open(json_file_path) as data_file:
        data = json.load(data_file)
        data_file.close()
        return data


def save_dict_to_json(json_file_path, dict_geo):
    with open(json_file_path, 'w') as data_file:
        json.dump(dict_geo, data_file)


def extract_iris_from_mobilipass():
    # extract data from mobilipass xsl file, and search iris (start, work, arrival) from the mentioned addresses,
    # and finally store the discovered iris in the file expes/profils_HiL_from_to.json
    # dans xsl : colonne Y=ancien domicile, AJ=nouveau domicile, AU=nouveautravail
    file_mobilipass = config.xls_data_HiL_file_path  # mobilipass xsl file
    profiles_hil = dict()
    fieldnames, data = xls_utils.parse_data_HiL_to_dict(file_mobilipass)
    nb_total_profiles = 0
    nb_profiles_France = 0  # start address in France
    nb_complete_profiles = 0  #

    db = Mongiris()
    db.init_connection()

    #TODO restore geocoder because db cannot retrieve a code iris from an addresse (see errors below)

    for id_profile, profile in data.iteritems():  # for each profile, get the adresses and use geocoder API to obtain IRIS codes

        nb_total_profiles += 1
        if profile['Ancien domicile pays'].strip() != "France":  # cannot find IRIS for addresses outside of France
            iris_start = None
        else:
            nb_profiles_France += 1
            address_start = profile['Ancien domicile adresse'].encode("utf-8")
            url = geocoder.build_url_from_address(address_start)
            iris_start = geocoder.get_code_iris_from_address(url)

        address_work = profile['Nouveau lieu de travail adresse'].encode("utf-8")
        address_arrival = profile['Nouveau domicile adresse'].encode("utf-8")
        url = geocoder.build_url_from_address(address_work)
        iris_work = geocoder.get_code_iris_from_address(url)
        url = geocoder.build_url_from_address(address_arrival)
        iris_arrival = geocoder.get_code_iris_from_address(url)

        if iris_start is not None and iris_work is not None and iris_arrival is not None:
            nb_complete_profiles += 1

        if iris_start is None:
            iris_start = ""
        if iris_work is None:
            iris_work = ""
        if iris_arrival is None:
            iris_arrival = ""

        print("#", id_profile, " :", iris_start, "\t", iris_arrival, "\t", iris_work)
        profiles_hil[id_profile] = dict()
        profiles_hil[id_profile]["code_iris_start"] = iris_start
        profiles_hil[id_profile]["code_iris_arrival"] = iris_arrival
        profiles_hil[id_profile]["code_iris_work"] = iris_work

    save_dict_to_json(config.json_HiL_profils_from_to, profiles_hil)
    print(profiles_hil)
    print("#total profiles = " + str(nb_total_profiles))
    print("#France profiles = " + str(nb_profiles_France))
    print("#complete profiles = " + str(nb_complete_profiles))