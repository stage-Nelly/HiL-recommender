/*
* Functions for Leaflet.js cartographic library (init Leaflet map, load a GeoJSON file as a layer, etc.)
* Only runs on Firefox without the server (cross origin requests error with other browsers)
*/


/******************* GLOBAL VARIABLES *******************/

let m = null; // leaflet map
let baseLayers = null; // array of basic layers
let overlayLayers = null; // array of overlaying layers
let osmLayer = null; // openstreetmap basic layer 
//let irisLayer = null; // IRIS overlay layer
// let communesLayer = null; // communes of Lyon metropole
//let irisToLayer = {}; // a dict such as {code_iris1: {layer}, code_iris2: {layer}, ...}
let departuresToLayer = {};
let arrivalToLayer = {};
let resetFeatureColors = true; // indicates whether color is reset or not
let aMarkers = new Array(); // a list to store markers placed on the map
let markerLayers = null; // a layer for markers
let aClusters = new Array(); // a list to store clusters placed on the map
let clusterLayers = null; // a layer for clusters
let newCircle = null;
let newMarker = null;
let newCluster = null;
let iconDeparture = null;
let iconArrival = null;
let nbMarkers = 0;
let nbClusters = 0;
let addCircle = true;
let layer = null;
let savedMarkers = [];
let savedClusters = [];
let jsonRecommendations = null;
let currentArrivals = null;
let currentDepartures = null;
let layersControl = null;
let continueExec = false;
let departureLayer = null;
let arrivalLayer = null;
let nameJustifications = {
    "animation-divertissement": "Divertissements",
    "animation-culturel": "Culturel",
    "animation-commerce-nonalimentaire": "Commerces non alimentaires",
    "animation-commerce-alimentaire-proximite": "Commerces alimentaires de proximité",
    "animation-commerce-alimentaire-grandesurface": "Grandes surfaces",
    "loisir": "Loisirs",
    "service-emploi": "Emploi",
    "service-justice": "Justice",
    "service-divers-prive": "Services privés",
    "service-divers-public": "Services publics",
    "service-sante": "Santé",
    "service-actionsociale": "Action sociale",
    "transport-longuedistance": "Transports",
    "transport-velo": "Vélo",
    "transport-busmetrotram": "Transports en commun",
    "securite": "Sécurité",
    "education-creche": "Crèche",
    "education-primaire-prive": "Enseignement primaire privé",
    "education-primaire-public": "Enseignement primaire public",
    "education-secondaire-cycle1-prive": "Enseignement secondaire privé (cycle 1)",
    "education-secondaire-cycle1-public": "Enseignement secondaire public (cycle 1)",
    "education-secondaire-cycle2-general-prive": "Enseignement secondaire général privé (cycle 2)",
    "education-secondaire-cycle2-general-public": "Enseignement secondaire général public (cycle 2)",
    "education-secondaire-cycle2-professionnel-prive": "Enseignement secondaire professionnel privé (cycle 2)",
    "education-secondaire-cycle2-professionnel-public": "Enseignement secondaire professionnel public (cycle 2)",
    "education-superieur-prive": "Enseignement supérieur privé",
    "education-superieur-public": "Enseignement supérieur public",
    "csp": "Classes socio-professionnelles",
    "espacevert": "Espaces verts",
    "logement-type": "Logements",
    "logement-residence": "Résidences",
    "logement-annee": "Années des logements",
    "logement-resident": "Résidents",
    "C14_ACT1564": "Actifs 15-64 ans (professions intermédiaires)",
    "C14_ACTOCC1564": "Actifs 15-64 ans (ouvriers)",
    "C14_ACTOCC15P": "Actifs 15 ans ou plus",
	"C14_POP15P": "Population 15 ans ou plus",
	"C14_POP15P_CS1": "Population (Agriculteurs exploitants)",
	"C14_POP15P_CS2": "Population (Artisans, Commerçants, Chefs d'entreprise)",
	"C14_POP15P_CS3": "Population (Cadres, Professions intellectuelles supérieures)",
	"C14_POP15P_CS4": "Population (Professions intermédiaires)",
	"C14_POP15P_CS5": "Population (Employés)",
	"C14_POP15P_CS6": "Population (Ouvriers)",
	"C14_POP15P_CS7": "Population (Retraités)",
	"C14_POP15P_CS8": "Population (Autres)",
	"DEC_D114": "1er décile (€)",
	"DEC_D214": "2e décile (€)",
	"DEC_D314": "3e décile (€)",
	"DEC_D414": "4e décile (€)",
	"DEC_D614": "6e décile (€)",
	"DEC_D714": "7e décile (€)",
	"DEC_D814": "8e décile (€)",
	"DEC_D914": "9e décile (€)",
	"P14_POP": "Population"
};
let justifications = "";
let type = "";
let start = false;

/******************* UTILS FOR MAP *******************/

/**
 * Reset the style of (previously) highlighted layer element
 * @param {event object} e .
 */
function resetHighlight(e) {
	if(resetFeatureColors)
		if(departureLayer["layers"]) {
			departureLayer["layers"].resetStyle(e.target);
		}
		if(arrivalLayer["layers"]) {
			arrivalLayer["layers"].resetStyle(e.target);
		}
}

/**
 * Reset the style of all highlighted layer elements
 */
function resetHighlightAll() {
	$.each(departureLayer["layers"], function(key, value) {
		console.log(departureLayer["layers"] + " : " + key + " ; " + value);
		if(departureLayer["layers"]) {
			departureLayer["layers"].resetStyle(value);
		}
	});
	$.each(arrivalLayer["layers"], function(key, value) {
		console.log(arrivalLayer["layers"] + " : " + key + " ; " + value);
		if(arrivalLayer["layers"]){
			arrivalLayer["layers"].resetStyle(value);
		}
	});
}

/**
 * Remove choices from select
 */
function removeSelect() {
	if(document.URL.indexOf("recommendation.html") >= 0){
		selectAlgoRec = document.getElementById("selectAlgoRec");
		topReco = document.getElementById("topReco");
		distReco = document.getElementById("distReco");
		if(selectAlgoRec) {
			selectAlgoRec.selectedIndex = 0;
		}
		if(topReco) {
			topReco.value = "";
		}
		if(distReco) {
			distReco.value = "";
		}
	} else if(document.URL.indexOf("clustering.html") >= 0){
		selectAlgoClust = document.getElementById("selectAlgoClust");
		if(selectAlgoClust) {
			selectAlgoClust.selectedIndex = 0;
		}
	}
}

/**
 * Set style for highlighted layer element
 * @param {event object} e .
 */
function highlightFeature(e) {
	if(resetFeatureColors) {
		var layer = e.target;
		layer.setStyle({
			weight: 1,
			color: '#666',
			fillOpacity: 0.25
		});
	}
}

/**
 * Click event on the IRIS layer: update the table to display information about selected IRIS
 * @param {event object} e .
 */
function clickProperties(e) {
    // creation of the indicators's table
	let tabProps = document.getElementById("properties-tab");
	let oldTbody = tabProps.querySelector('tbody');
	let newTbody = document.createElement('tbody');
	tabProps.replaceChild(newTbody, oldTbody);
	let props = e.target.feature.properties;

	key = "grouped_indicators";
	for (var key2 in props[key]) {
		if(nameJustifications[key2] != undefined) {
			let row = newTbody.insertRow(-1);
			row.setAttribute("class", "grouped", 0);
			row.insertCell(0).innerHTML = key2;
			row.insertCell(1).innerHTML = nameJustifications[key2];
			row.insertCell(2).innerHTML = props[key][key2];
		}
	}

	key = "raw_indicators";
	for (var key2 in props[key]) {
		if(dictIndicateurs[key2] != undefined) {
			let row = newTbody.insertRow(-1);
			row.setAttribute("class", "brut", 0);
			row.insertCell(0).innerHTML = key2;
			row.insertCell(1).innerHTML = dictIndicateurs[key2].long_fieldname;
			row.insertCell(2).innerHTML = props[key][key2];
		}
	}

	$("tr.grouped").show();
	$("#groupedTable").prop("checked", true);
	$("tr.brut").hide();
	$("#brutTable").prop("checked", false);
}

$('#groupedTable').change(function () {
    if (!this.checked)
        $('tr.grouped').hide();
    else
        $('tr.grouped').show();
});

$('#brutTable').change(function () {
    if (!this.checked)
        $('tr.brut').hide();
    else
        $('tr.brut').show();
});


/**
 * Add tooltip, mouseover, mouseout and click events to the (features of) IRIS layer
 * @param {number} feature .
 * @param {layer} layer .
 */
function eventsIRIS(feature, layer) {
	layer.on({
		mouseover: highlightFeature,
		mouseout: resetHighlight,
		click: clickProperties
	});
	if (type == 'departure') {
		departuresToLayer[feature.properties.CODE_IRIS] = layer;
	} else if (type == 'arrival') {
		arrivalToLayer[feature.properties.CODE_IRIS] = layer;
	}
	// setting tooltips
	if (feature.properties.nom) {
		layer.bindTooltip(feature.properties.nom);
	} else if (feature.properties.CODE_IRIS) {
	    messageTooltip = "IRIS : " + feature.properties.NOM_IRIS + "<br/>COMMUNE : " + feature.properties.NOM_COM;
        if(jsonRecommendations["recommendations"][feature.properties.CODE_IRIS] != undefined) {
            if(jsonRecommendations["typeAlgo"] != 'clustering') // add the confidence score in the tootip
                messageTooltip += "<br/>Recommand&eacute; &agrave; <b>" + ((jsonRecommendations["recommendations"][feature.properties.CODE_IRIS])*100).toFixed(2) + "%</b>"; // toFixed : rounded to two numbers
            else // add the cluster id and total number of clusters for clustering
                messageTooltip += "<br/>Cluster #" + jsonRecommendations["recommendations"][feature.properties.CODE_IRIS] + " (sur " + jsonRecommendations["nb_clusters"] + ")";
        }
        /*
		    /*  comment Fab - plantage des justifications
			try {
                justifications = "<ul>";
                if(jsonRecommendations["justifications"]) {
                    console.log("there are iris which are not null");
                    for (i = 0 ; i < jsonRecommendations["justifications"][feature.properties.CODE_IRIS].length ; i++) {
                        justifications += "<li>" + nameJustifications[jsonRecommendations["justifications"][feature.properties.CODE_IRIS][i]] + "</li>";
                    }
                    justifications += "</ul>";
                    layer.bindTooltip("IRIS : " + feature.properties.NOM_IRIS + "<br/>COMMUNE : " + feature.properties.NOM_COM + "<br/>Recommand&eacute; &agrave; <b>" + ((jsonRecommendations["recommendations"][feature.properties.CODE_IRIS])*100).toFixed(2) + "%</b><br/>Indicateurs marquants : <br/>" + justifications); // toFixed : rounded to two numbers
                } else {
                    layer.bindTooltip("IRIS : " + feature.properties.NOM_IRIS + "<br/>COMMUNE : " + feature.properties.NOM_COM + "<br/>Recommand&eacute; &agrave; <b>" + ((jsonRecommendations["recommendations"][feature.properties.CODE_IRIS])*100).toFixed(2) + "%</b>"); // toFixed : rounded to two numbers
                }
            } catch(error) {
                $("#loading").hide();
                if(document.getElementById("danger") == null) {
                    $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\" style=\"width: 75%;\">L'affichage du résultat a recontré un problème, veuillez recommencer...</div>");
                }
            }
		}*/
		layer.bindTooltip(messageTooltip); // adding the tooltip
	}
}

/**
 * Add IRIS layer from GeoJSON data
 * @param {geojson} geojson .
 * @param {event} events .
 * @param {object} style .
 * @param {function} callback .
 */
function addLayerFromGeoJSON(geojson, events, style){
	if(type == 'departure') {
		departureLayer = new L.geoJSON(geojson, {onEachFeature: events});
		departureLayer.setStyle(style);
		departureLayer.addTo(m);
		return departureLayer;
	} else if(type == 'arrival') {
		arrivalLayer = new L.geoJSON(geojson, {onEachFeature: events});
		arrivalLayer.setStyle(style);
		arrivalLayer.addTo(m);
		return arrivalLayer;
	}
}

/***************** MARKERS AND CLUSTERS *****************/

/**
 * Add marker when click on the map
 * @param {event} e .
 */
function addMarker(e) {
	if($("#selectAlgoRec").val() != '') {
		markerLayers = L.layerGroup();

        if($("#selectAlgoRec").val() != 'deviation' && nbMarkers >= 2) { // block after 2 markers for all algorithms except deviation
			m.off('click'); // disable click for many IRIS in case of cosine similarity (one start IRIS)
		} else if($("#selectAlgoRec").val() == 'deviation' && start == true) { // block when start recommendation or clustering
		    m.off('click');
		} else {
			if($("#selectAlgoRec").val() != 'deviation') { // for later checking number of markers for cosine, clustering, etc.
				nbMarkers++;
			}
			if(e["type"] == "contextmenu" && addCircle == true) { // right click => new arrival
				iconArrival = L.icon({iconUrl: 'static/img/arrival.png', iconSize: [25, 41], iconAnchor: [12, 41]});
				newMarker = new L.marker(e.latlng, {icon: iconArrival, draggable: false}).addTo(m); // creating a new marker
				newCircle = new L.Circle(e.latlng, {radius: 1000*parseInt(getDistance($("#distReco").val())), color: 'black', fillColor: 'black'}).addTo(m); // radius in meters
				aMarkers.push({'coords': newMarker.getLatLng(), 'type': 'end'});
				addCircle = false;
				savedMarkers.push(newMarker);
			} else { // left click ==> one or many departure(s)
				iconDeparture = L.icon({iconUrl: 'static/img/departure.png', iconSize: [25, 41], iconAnchor: [12, 41]});
				newMarker = new L.marker(e.latlng, {icon: iconDeparture, draggable: false}).addTo(m); // creating a new marker
				aMarkers.push({'coords': newMarker.getLatLng(), 'type': 'start'});
				savedMarkers.push(newMarker);
			}
			return aMarkers;
		}
	}
}

/**
 * Add cluster when click on the map
 * @param {event} e .
 */
function addCluster(e) {
	if($("#selectAlgoClust").val() != '') {
		clusterLayers = L.layerGroup();
		clusterLayers.addTo(m);

		if (nbClusters == 0) {
			iconDeparture = L.icon({iconUrl: 'static/img/departure.png', iconSize: [25, 41], iconAnchor: [12, 41]});
			newCluster = new L.marker(e.latlng, {icon: iconDeparture}).addTo(m); // creating a new marker for the work IRIS
			newCircle = new L.Circle(e.latlng, {radius: 1000*parseInt(getDistance($("#distClust").val())), color: 'black', fillColor: 'black'}).addTo(m); // radius in meters
			aClusters.push(newCluster.getLatLng());
			savedClusters.push(newCluster);
		} else {
			m.off('click');
		}
		nbClusters++;
		return aClusters;
	}
}

function markersToLayers() {
	markerLayers = L.layerGroup(savedMarkers);
	markerLayers.addTo(m);
}

function clustersToLayers() {
	clusterLayers = L.layerGroup(savedClusters);
	clusterLayers.addTo(m);
}

function loadLayers() {
	storeDictIndicateurs("../static/data/dictionnaire-indicateurs.json"); // get the JSON dictionary for indicators
	type = 'departure';
	departureLayer = addLayerFromGeoJSON(currentDepartures, eventsIRIS, {style : {color: "#000000", fillColor: "#000000", fillOpacity: 0}});
	type = 'arrival';
	arrivalLayer = addLayerFromGeoJSON(currentArrivals, eventsIRIS, {style : {color: "#000000", fillColor: "#000000", fillOpacity: 0}});
	overlayLayers = {'departure IRIS' : departureLayer, 'arrival IRIS': arrivalLayer};
	continueExec = true;
}

/*************** INITIALIZATION OF THE MAP ***************/

/**
 * Initialize the map
 */
function initialize() {
	if(document.URL.indexOf("recommendation.html") >= 0 || document.URL.indexOf("clustering.html") >= 0){
		m = L.map("map-id").setView([46.895143, 2.397461], 6); // creation of the map and general settings : centering and zoom level

		// creation of an OSM layer
		osmLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			"attribution": 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
			"maxZoom": 18,
			"detectRetina": false,
			"minZoom": 1,
			"noWrap": false,
			"subdomains": "abc"
		}).addTo(m);

		// legend for layers
		baseLayers = {'OpenStreetMap' : osmLayer};

		if(document.URL.indexOf("recommendation.html") >= 0){
			m.on('contextmenu', function(e) { // if right click on the map, add marker + circle
			    addMarker(e);
			});
		    m.on('click', addMarker); // else add marker
		}
		if(document.URL.indexOf("clustering.html") >= 0){
			m.on('contextmenu', function(e) { // if right click on the map, add marker + circle
			    addCluster(e);
			});
			m.on('click', addCluster); // else add cluster
		}
	}
}