/*
* Functions for Leaflet.js cartographic (init Leaflet map, load a GeoJSON file as a layer, etc.)
* Runs on Firefox (not in Safari nor Chrome because cannot load local files due to cross origin requests)
* Server http : python -m SimpleHTTPServer
*/

/**
  * a for array
  * s for string
  * i for integer
  * f for float
  * d for dictionary
  * b for boolean
**/


/******************* GLOBAL VARIABLES *******************/

let dictIndicateurs = null; // a JSON string containing information about indicators
let aCodesIrisReco = []; // an array to store the codes of IRIS (pointed by markers)
let aCodesIrisClust = []; // an array to store the codes of IRIS (pointed by clusters)
let aNeighborhood = []; // an array to store the codes of IRIS of neighborhood


/************************* CSS *************************/

/* code for adding padding top to the body so that the navbar does not hide content */
$('body').css('padding-top', parseInt($('nav').css("height"))+10);

$(window).resize(function () {
	$('body').css('padding-top', parseInt($('nav').css("height"))+10);
});


/************************ UTILS ************************/

/**
 * Reads a local JSON file and apply callback on the JSON string.
 * @param {string} file .
 * @param {function} callback .
 */
function readJSONFile(file, callback) {
	var rawFile = new XMLHttpRequest();
	rawFile.overrideMimeType("application/json");
	rawFile.open("GET", file, true);
	rawFile.onreadystatechange = function() {
		if (rawFile.readyState === 4 && rawFile.status == "200") {
			callback(rawFile.responseText);
		}
	}
	rawFile.send(null);
}

function storeDictIndicateurs(file) {
	readJSONFile(file, function(text){
		dictIndicateurs = JSON.parse(text);
	});
}


/************************ UTILS ************************/

$("#buttonResetIris").click(function(e) {
	e.preventDefault();
	resetFeatureColors = true;
	resetHighlightAll();
	if(document.URL.indexOf("recommendation.html") >= 0){
		for(var i = 0; i < aMarkers.length; i++){
		   m.removeLayer(aMarkers[i]);
		}
		if(markerLayers) {
			markerLayers.clearLayers();
			markerLayers = null;
		}
		aMarkers = [];
		nbMarkers = 0;
		savedMarkers = [];
		aCodesIrisReco = [];
		addCircle = true;
		m.removeLayer(departureLayer);
		departureLayer = null;
	}
	if(document.URL.indexOf("clustering.html") >= 0){
		if(clusterLayers) {
			m.removeLayer(clusterLayers);
			clusterLayers = null
		}
		aClusters = [];
		for(var i = 0; i < aMarkers.length; i++){
		   m.removeLayer(aMarkers[i]);
		}
		aClusters = [];
		nbClusters = 0;
		savedClusters = [];
		aCodesIrisClust = [];
	}
	aNeighborhood = [];
	layer = null;
	m.removeLayer(newCircle);
	m.removeLayer(arrivalLayer);
	newMarker = null;
	overlayLayers = null; // array of overlaying layers
	continueExec = false;
	arrivalLayer = null;
	currentArrivals = {};
	dictIndicateurs = {};
	removeSelect();
});

function getColor(d) {
	return d > 0.95 ? '#005824' :
		   d > 0.9  ? '#f7fcfd' : // #238b45
		   d > 0.8  ? '#41ae76' :
		   d > 0.7  ? '#66c2a4' :
		   d > 0.6  ? '#99d8c9' :
		   d > 0.5  ? '#ccece6' :
		   d > 0.25 ? '#e5f5f9' :
					  '#f7fcfd';
}

function colorIris(recommendations, type) {
	$.each(recommendations, function(key, value) {
		let l;
		if(type == "reco") {
			l = arrivalToLayer[value];
 		} else if (type == "dict") {
 		    l = arrivalToLayer[key];
 		}

		l.setStyle({
			weight: 1,
			color: getColor(value),
			fillOpacity: 0.4
		});
	});
}

function arrayIrisToString(aIris, bObj) {
	sCodes = "";
	if(bObj == false) { // aIris contains IRIS codes
		for(i = 0 ; i < aIris.length; i++) {
			if(i == aIris.length-1) {
				sCodes += aIris[i]; // to avoid _ at the end of the string
			} else {
				sCodes += aIris[i]+"_";
			}
		}
	} else { // aIris contains IRIS
		for(i = 0 ; i < aIris[0].length ; i++) {
			if(i == aIris[0].length-1) {
				sCodes += aIris[0][i]["properties"]["CODE_IRIS"]; // to avoid _ at the end of the string
			} else {
				sCodes += aIris[0][i]["properties"]["CODE_IRIS"] + "_";
			}
		}
	}
	return sCodes;
}

function getTop(sTop) {
	if(sTop == "" || !sTop) {
		return "10"; // top 10 by default
	} else {
		return ""+parseInt(sTop); // parseInt returns int if int, returns int if float (10.1 returns 10)
	}
}

function getDistance(sDistance) {
	if(sDistance == "" || !sDistance) {
		return "3"; // 3 kilometers by default
	} else {
		return ""+parseInt(sDistance); // parseInt returns int if int, returns int if float (3.1 returns 3)
	}
}

function sortByKey(array, key) {
	return array.sort(function(elem1, elem2) {
		var id1 = elem1[key];
		var id2 = elem2[key];
		return ((id1 < id2) ? -1 : ((id1 > id2) ? 1 : 0));
	});
}


/****************** RECOMMENDATION ******************/
/**
  * Do all AJAX requests to recommend
  * @param{event} e .
**/
$("#buttonColorRecommendations").click(function(e) {
    start = true;
    if(aMarkers.length > 1) { // at least two markers
		$("#loading").css("display", "inline-block");
	    $("#loading").show();
	    markersToLayers(); // adding all markers to a layerGroup()
		aCodesIrisReco = []; // codes of iris pointed by markers
		for(i = 0; i < aMarkers.length; i++) {
			// getting latitude and longitude of each marker ...
			marker = ""+aMarkers[i]['coords'];
			sLat = marker.substring(7, marker.indexOf(', '));
			sLng = marker.substring(marker.indexOf(', ')+2, marker.length-1);
			sType = aMarkers[i]['type'];
			// ... and converting it into IRIS code
			$.ajax({
				type: "GET",
				url: "/geocoding",
				data: {
					'lat': sLat,
					'lng': sLng
				},
				//async : false, // don't use it, obsolete
				ajaxType: aMarkers[i]['type'],
				contentType: 'application/json;charset=UTF-8',
				success: function(codeIris) {
					typeMarker = this.ajaxType; // current value of type of the current marker
					if(codeIris != null) {
						aCodesIrisReco.push({'code_iris': codeIris, 'type': typeMarker}); // store IRIS's code of each marker
					} else {
						console.log("Cet IRIS n'a pas de donnée !");
						$("#loading").hide();
                        if(document.getElementById("danger") == null) {
                            $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\">Cet IRIS est inconnu, veuillez recommencer...</div>");
                        }
					}
				},
				complete: function(){
					// recommend only when all codes are calculated
					// first condition to avoid that it begins to execute recommendation before the completion of all Ajax requests
					if(aCodesIrisReco.length == aMarkers.length) {
						var temp = aCodesIrisReco;
						aCodesIrisReco = [];
						// adding the end iris (the area of recommendations) in first position of the table
						for(i = 0 ; i < temp.length ; i++) {
							if(temp[i]['type'] == 'end'){
								aCodesIrisReco.push(temp[i]['code_iris']);
								break; // only one end iris, so exit the loop when found
							}
						}
						// adding the departure(s) after
						for(i = 0 ; i < temp.length ; i++) {
							if(temp[i]['type'] == 'start') {
								aCodesIrisReco.push(temp[i]['code_iris']);
							}
						}
						console.log("iris start and end = " + aCodesIrisReco);
						console.log("iris end = " + aCodesIrisReco[0]);
						buildNeighborhood(aCodesIrisReco[0], getDistance($("#distReco").val()), "reco"); // build the neighborhood of the arrival iris at distReco kms
					}
				},
				error: function(textStatus, errorThrown) {
					$("#loading").hide();
                    if(document.getElementById("danger") == null) {
                        $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\">Un ou plusieurs marqueurs n'ont pas pu être géolocalisés, veuillez recommencer...</div>");
                    }
                    console.log(errorThrown);
				}
			});
		}
	} else {
		$("#noMarker").modal("show");
	}
});

/**
  * Call the AJAX request to computes the cosine measure
  * @param{array} aCodesIris an array of IRIS codes for which the cosine measure will be performed
  * @return the json of recommendations
**/
function recommendCosine(aCodesIris) {
	console.log("recommendation cosine...");
	$.ajax({
		type: "GET",
		url: "/cosine",
		data: {
			'code_iris': aCodesIris[1], // the departure iris
			'candidates': arrayIrisToString(aNeighborhood, true),
			'top': getTop($("#topReco").val())
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result){
			//resetHighlightAll();
			jsonRecommendations = JSON.parse(result);
			console.log("recommendations = " + jsonRecommendations["recommendations"]);
			//console.log("justifications = " + jsonRecommendations["justifications"]);
			currentArrivals = jsonRecommendations["current_arrivals"];
			currentDepartures = jsonRecommendations["current_departures"];
			typeAlgo = jsonRecommendations["typeAlgo"]; // cosine, svm, clustering, std
			resetFeatureColors = false;

			loadLayers(); // color recommendations after all layers are loaded
			if(continueExec == true) {
				colorIris(jsonRecommendations["recommendations"], "dict");
				for(i = 1 ; i < aCodesIris.length; i++) {
					departuresToLayer[aCodesIris[i]].setStyle({ // color iris for which recommendations are performed
						weight: 1,
						color: 'blue',
						fillOpacity: 0.5
					});
				}
			}
			if(layersControl != undefined && layersControl != null) {
				m.removeControl(layersControl);
			}
			layersControl = L.control.layers(baseLayers, overlayLayers).addTo(m);
			$("#loading").hide();
			$("#informations").append("<div id=\"succeed\" class=\"alert alert-success\">La recommendation est terminée !</div>");
			setTimeout(function() {
				$("#succeed").remove();
			}, 5000);
		},
		error: function(result, textStatus, errorThrown) {
			$("#loading").hide();
			if(document.getElementById("danger") == null) {
                $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\">La recommandation a rencontré un problème, veuillez recommencer...</div>");
            }
            console.log(errorThrown);
		}
	});
}

function recommendDeviation(aCodesIris) {
	console.log("recommendation std...");
	codes = arrayIrisToString(aCodesIris, false); // create a string to send many IRIS codes (i.e. code1_code2_code2...)
	$.ajax({
		type: "GET",
		url: "/deviation",
		data: {
			'codes_iris': codes,
			'candidates': arrayIrisToString(aNeighborhood, true),
			'top': getTop($("#topReco").val())
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result){
			//resetHighlightAll();
			jsonRecommendations = JSON.parse(result);
			console.log(jsonRecommendations["recommendations"]);
			currentArrivals = jsonRecommendations["current_arrivals"];
			currentDepartures = jsonRecommendations["current_departures"];
			typeAlgo = jsonRecommendations["typeAlgo"]; // cosine, svm, clustering, std
			resetFeatureColors = false;

			// color start iris
			loadLayers(); // color recommendations after all layers are loaded
			if(continueExec == true) {
				colorIris(jsonRecommendations["recommendations"], "dict");
				for(i = 1 ; i < aCodesIris.length; i++) {
					departuresToLayer[aCodesIris[i]].setStyle({ // color iris for which recommendations are performed
						weight: 1,
						color: 'blue',
						fillOpacity: 0.5
					});
				}
			}

			if(layersControl != undefined && layersControl != null) {
				m.removeControl(layersControl);
			}
			layersControl = L.control.layers(baseLayers, overlayLayers).addTo(m);
			$("#loading").hide();
			$("#informations").append("<div id=\"succeed\" class=\"alert alert-success\">La recommendation est terminée !</div>");
			setTimeout(function() {
				$("#succeed").remove();
			}, 5000);
		},
		error: function(result, textStatus, errorThrown) {
			$("#loading").hide();
			if(document.getElementById("danger") == null) {
                $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\">La recommendation a rencontré un problème, veuillez recommencer...</div>");
            }
            console.log(errorThrown);
		}
	});
}

function recommendClustering(aCodesIris) {
	console.log("recommendation algo clustering...");
	$.ajax({
		type: "GET",
		url: "/clusteringReco",
		data: {
			'algo_clust': $("#selectAlgoRec").val(),
			'code_iris': aCodesIris[1], // the departure iris
			'candidates': arrayIrisToString(aNeighborhood, true),
			'top': getTop($("#topReco").val())
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result) {
			//resetHighlightAll();
			jsonRecommendations = JSON.parse(result);
			console.log(jsonRecommendations["recommendations"]);
			//console.log(jsonRecommendations["justifications"]);
			currentArrivals = jsonRecommendations["current_arrivals"];
			currentDepartures = jsonRecommendations["current_departures"];
			typeAlgo = jsonRecommendations["typeAlgo"]; // cosine, svm, clustering, std
			resetFeatureColors = false;

			// color recommendations
			loadLayers(); // color recommendations after all layers are loaded
			if(continueExec == true) {
				var rainbow = new Rainbow();
				rainbow.setNumberRange(0, jsonRecommendations["nb_clusters"]);
				rainbow.setSpectrum('white', 'lemonchiffon', 'lightsalmon', 'orange', 'red', 'mediumorchid', 'darkviolet',
				'cornflowerblue', 'cyan', 'blue', 'lightseagreen', 'mediumspringgreen', 'limegreen', 'olive',
				'peru', 'saddlebrown', 'maroon', 'lightslategrey', 'dimgray', 'black');
				$.each(jsonRecommendations["recommendations"], function(key, value) {
					let l = arrivalToLayer[key];
					let c = '#' + rainbow.colourAt(value);
					l.setStyle({
						weight: 1,
						color: c,
						fillOpacity: 0.7
					 });
				});
				for(i = 1 ; i < aCodesIris.length; i++) {
					departuresToLayer[aCodesIris[i]].setStyle({ // color iris for which recommendations are performed
						weight: 1,
						color: 'blue',
						fillOpacity: 0.5
					});
				}
				if(layersControl != undefined && layersControl != null) {
					m.removeControl(layersControl);
				}
				layersControl = L.control.layers(baseLayers, overlayLayers).addTo(m);
				$("#loading").hide();
				$("#informations").append("<div id=\"succeed\" class=\"alert alert-success\">La recommendation est terminée !</div>");
                setTimeout(function() {
                    $("#succeed").remove();
                }, 5000);
			}
		},
		error: function(result, textStatus, errorThrown) {
			$("#loading").hide();
			if(document.getElementById("danger") == null) {
                $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\">La recommendation a rencontré un problème, veuillez recommencer...</div>");
            }
            console.log(errorThrown);
		}
	});
}

function recommendHdbscan(aCodesIris) {
	console.log("recommendation hdbscan...");
	$.ajax({
		type: "GET",
		url: "/recommendHdbscan",
		data: {
			'code_iris': aCodesIris[1], // the departure iris
			'candidates': arrayIrisToString(aNeighborhood, true),
			'top': getTop($("#topReco").val())
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result) {
			//resetHighlightAll();
			jsonRecommendations = JSON.parse(result);
			console.log(jsonRecommendations["recommendations"]);
			//console.log(jsonRecommendations["justifications"]);
			currentArrivals = jsonRecommendations["current_arrivals"];
			currentDepartures = jsonRecommendations["current_departures"];
			typeAlgo = jsonRecommendations["typeAlgo"]; // cosine, svm, clustering, std
			resetFeatureColors = false;

			// color recommendations
			loadLayers(); // color recommendations after all layers are loaded
			if(continueExec == true) {
				var rainbow = new Rainbow();
				rainbow.setNumberRange(0, jsonRecommendations["nb_clusters"]);
				rainbow.setSpectrum('white', 'lemonchiffon', 'lightsalmon', 'orange', 'red', 'mediumorchid', 'darkviolet',
				'cornflowerblue', 'cyan', 'blue', 'lightseagreen', 'mediumspringgreen', 'limegreen', 'olive',
				'peru', 'saddlebrown', 'maroon', 'lightslategrey', 'dimgray', 'black');
				$.each(jsonRecommendations["recommendations"], function(key, value) {
					let l = arrivalToLayer[key];
					let c = '#' + rainbow.colourAt(value);
					l.setStyle({
						weight: 1,
						color: c,
						fillOpacity: 0.7
					 });
				});
				for(i = 1 ; i < aCodesIris.length; i++) {
					departuresToLayer[aCodesIris[i]].setStyle({ // color iris for which recommendations are performed
						weight: 1,
						color: 'blue',
						fillOpacity: 0.5
					});
				}
				if(layersControl != undefined && layersControl != null) {
					m.removeControl(layersControl);
				}
				layersControl = L.control.layers(baseLayers, overlayLayers).addTo(m);
				$("#loading").hide();
				$("#informations").append("<div id=\"succeed\" class=\"alert alert-success\">La recommendation est terminée !</div>");
                setTimeout(function() {
                    $("#succeed").remove();
                }, 5000);
			}
		},
		error: function(result, textStatus, errorThrown) {
			$("#loading").hide();
			if(document.getElementById("danger") == null) {
                $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\">La recommendation a rencontré un problème, veuillez recommencer...</div>");
            }
            console.log(errorThrown);
		}
	});
}


function recommendSvm(aCodesIris) {
	console.log("recommendation svm...");
	$.ajax({
		type: "GET",
		url: "/svm",
		data: {
			'code_iris': aCodesIris[1], // the departure iris
			'candidates': arrayIrisToString(aNeighborhood, true)
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result) {
			//resetHighlightAll();
			jsonRecommendations = JSON.parse(result);
			console.log(jsonRecommendations["recommendations"]);
			currentArrivals = jsonRecommendations["current_arrivals"];
			currentDepartures = jsonRecommendations["current_departures"];
			typeAlgo = jsonRecommendations["typeAlgo"]; // cosine, svm, clustering, std
			resetFeatureColors = false;

			// color recommendations
			loadLayers(); // color recommendations after all layers are loaded
			if(continueExec == true) {
				colorIris(jsonRecommendations["recommendations"], "dict");
				for(i = 1 ; i < aCodesIris.length; i++) {
					departuresToLayer[aCodesIris[i]].setStyle({ // color iris for which recommendations are performed
						weight: 1,
						color: 'blue',
						fillOpacity: 0.5
					});
				}
				if(layersControl != undefined && layersControl != null) {
					m.removeControl(layersControl);
				}
				layersControl = L.control.layers(baseLayers, overlayLayers).addTo(m);
				$("#loading").hide();
				$("#informations").append("<div id=\"succeed\" class=\"alert alert-success\">La recommendation est terminée !</div>");
                setTimeout(function() {
                    $("#succeed").remove();
                }, 5000);
			}
		},
		error: function(result, textStatus, errorThrown) {
			$("#loading").hide();
			if(document.getElementById("danger") == null) {
                $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\">La recommendation a rencontré un problème, veuillez recommencer...</div>");
            }
            console.log(errorThrown);
		}
	});
}

/******************** CLUSTERING ********************/
$("#buttonColorClusters").click(function(e) {
    start = true;
    if(aClusters.length > 0) {
        $("#loading").css("display", "inline-block");
	    $("#loading").show();
	    clustersToLayers();
        var j = 0;
        aCodesIrisClust = [];
        for(i = 0; i < aClusters.length; i++) { // first data is the start IRIS, the other(s) is(are) IRIS work
            // getting latitude and longitude...
            clust = ""+aClusters[i];
            sLat = clust.substring(7, clust.indexOf(', '));
            sLng = clust.substring(clust.indexOf(', ')+2, clust.length-1);
            // ... and converting it into IRIS code
            $.ajax({
                type: "GET",
                url: "/geocoding",
                data: {
                    'lat': sLat,
                    'lng': sLng
                },
                contentType: 'application/json;charset=UTF-8',
                success: function(codeIris){
                    aCodesIrisClust.push(codeIris); // store IRIS's code
                },
                complete: function(){
                    // first condition to avoid that it begins to execute clustering before the completion of all Ajax requests
                    if(aCodesIrisClust.length == aClusters.length) {
                        buildNeighborhood(aCodesIrisClust[0], getDistance($("#distClust").val()), "clust");
                    }
                },
                error: function(textStatus, errorThrown) {
                    $("#loading").hide();
                    if(document.getElementById("danger") == null) {
                        $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\">Les marqueurs n'ont pas pu être géolocalisés, veuillez recommencer...</div>");
                    }
                    console.log(errorThrown);
                }
            });
        }
    } else {
        $("#noMarker").modal("show");
    }
});

function clusterize(a_Iris, iris) {
    console.log("clustering...");
	codes = arrayIrisToString(a_Iris, true);
	$.ajax({
		type: "GET",
		url: "/clusteringClust",
		data: {
			'algo_clust': $("#selectAlgoClust").val(),
			'candidates': codes
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result) {
			//resetHighlightAll();
			jsonRecommendations = JSON.parse(result);
			console.log(jsonRecommendations["recommendations"]);
			currentArrivals = jsonRecommendations["current_arrivals"];
			resetFeatureColors = false;

			// color recommendations
			loadLayers();
			// color recommendations after all layers are loaded
			if(continueExec == true) {
				var rainbow = new Rainbow();
				rainbow.setNumberRange(0, jsonRecommendations["nb_clusters"]);
				rainbow.setSpectrum('white', 'lemonchiffon', 'lightsalmon', 'orange', 'red', 'mediumorchid', 'darkviolet',
				'cornflowerblue', 'cyan', 'blue', 'lightseagreen', 'mediumspringgreen', 'limegreen', 'olive',
				'peru', 'saddlebrown', 'maroon', 'lightslategrey', 'dimgray', 'black');
				$.each(jsonRecommendations["recommendations"], function(key, value) {
					let l = arrivalToLayer[key];
					let c = '#' + rainbow.colourAt(value);
					l.setStyle({
						weight: 1,
						color: c,
						fillOpacity: 0.7
					 });
				});
				if(layersControl != undefined && layersControl != null) {
					m.removeControl(layersControl);
				}
				layersControl = L.control.layers(baseLayers, overlayLayers).addTo(m);
				$("#loading").hide();
				$("#informations").append("<div id=\"succeed\" class=\"alert alert-success\">Le regroupement est terminé !</div>");
                setTimeout(function() {
                    $("#succeed").remove();
                }, 5000);
			}
		},
		error: function(result, textStatus, errorThrown) {
			$("#loading").hide();
			if(document.getElementById("danger") == null) {
                $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\">Le regroupement a rencontré un problème, veuillez recommencer...</div>");
            }
            console.log(errorThrown);
		}
	});
}


/****************** UTILS ****************************/
/**
  * Build the neighborhood of an IRIS at distance kms
  * @param{string} codeIris the code of the IRIS for which the neighborhood will be calculated
  * @param{string} distance the maximal distance for the neighborhood
  * @param{string} type "reco" for building the neighborhood for recommendation, "clust" for building the neighborhood for clustering
**/
function buildNeighborhood(sCodeIris, sDistance, sType) {
	console.log("build neighborhood of iris " + sCodeIris + "...");
	i = 0;

	if(sType == "reco") {
		/**
		  * Computes the neighborhood of an IRIS code
		  * @param{string} code_iris the code for which the neighborhood will be computed
		  * @param{string} distance the maximal distance between the considered IRIS and the neighbor
		  * @return{array} an array of the IRIS codes of each neighbors
		**/
		$.ajax({
			type: "GET",
			url: "/neighborhood",
			data: {
				'code_iris': sCodeIris,
				'dist': sDistance
			},
			contentType: 'application/json;charset=UTF-8',
			success: function(result){
				i = i+1;
				aNeighborhood.push(result); // store the codes of neighbors at dist km
			},
			complete: function(){
				// first condition to avoid that it begins to execute clustering before the completion of all Ajax requests
				if(i == 1) {
					if ($("#selectAlgoRec").val() == 'cosine') {
						recommendCosine(aCodesIrisReco);
					} else if ($("#selectAlgoRec").val() == 'deviation') {
						recommendDeviation(aCodesIrisReco);
					} else if ($("#selectAlgoRec").val() == 'svm') {
						recommendSvm(aCodesIrisReco);
					} else if ($("#selectAlgoRec").val() == 'hdbscan') {
						recommendHdbscan(aCodesIrisReco);
					} else {
						recommendClustering(aCodesIrisReco);
					}
				}
			},
			error: function(textStatus, errorThrown) {
				$("#loading").hide();
			    if(document.getElementById("danger") == null) {
                    $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\">La construction du voisinage a rencontré un problème, veuillez recommencer...</div>");
                }
                console.log(errorThrown);
			}
		});
	} else if(sType == "clust") {
		$.ajax({
			type: "GET",
			url: "/neighborhood",
			data: {
				'code_iris': sCodeIris,
				'dist': sDistance
			},
			contentType: 'application/json;charset=UTF-8',
			success: function(result){
				i = i+1;
				aNeighborhood.push(result); // store the neighborhood at dist km
			},
			complete: function(){
				// clusterize only when all codes are calculated
				// first condition to avoid that it begins to execute clustering before the completion of all Ajax requests
				if(i == aClusters.length) {
					clusterize(aNeighborhood, sCodeIris); // recommend only when all codes are calculated
				}
			},
			error: function(textStatus, errorThrown) {
				$("#loading").hide();
			    if(document.getElementById("danger") == null) {
                    $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\">La construction du voisinage a rencontré un problème, veuillez recommencer...</div>");
                }
                console.log(errorThrown);
			}
		});
	}
}

/******************** OLD ********************/

$("#buttonColorIris").click(function(e) {
	resetFeatureColors = false;
	$.each(irisToLayer, function(key, value) {
		var r = Math.floor(Math.random() * 255);
		var g = Math.floor(Math.random() * 255);
		var b = Math.floor(Math.random() * 255);
		value.setStyle({
			weight: 1,
			color: "rgb("+r+" ,"+g+","+ b+")",
			fillOpacity: 0.4
		});
	});
	irisToLayer["690670000"].setStyle({
		weight: 1,
		color: 'green',
		fillOpacity: 0.4
	});
});

