#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# An sequential integration process to integrate Hil data (json files) in
# the Excel file for sociologues (tableau-mobilpass)
# =============================================================================

import os
from vizliris import json_utils
import xlrd  # for old Excel files (.xls) and new files (.xlsx Excel 2010)

dir_hil = os.path.join('data', 'HiL')
dir_json_files = os.path.join(dir_hil, 'profils-HiL-2018-06-22')  # update for new version of HiL data
json_hosts = os.path.join(dir_json_files, 'hosts.json')  # annonces de logement
json_leases = os.path.join(dir_json_files, 'leases.json')  # souhaits des personnes (3 questions)
json_comments = os.path.join(dir_json_files, 'comments.json')  # commentaires sur annonces ?
json_proposals = os.path.join(dir_json_files, 'proposals.json')  # commentaires sur visites (?) + like / dislike

dir_socio = os.path.join('data', 'socio')
xlsx_data = os.path.join(dir_socio, 'tableau-mobilpass-2018-06-12-test.xlsx')


#################################
# processing hosts (annonces)

hosts = json_utils.parse_json_to_dict(json_hosts)
print(hosts)





#################################






