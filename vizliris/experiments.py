#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# Experiments.py enable to run complete experiments (selection of parameters,
# running recommendation algorithm(s), and evaluation)
# =============================================================================

import config
import json_utils
import geo_utils
import recommender
import evaluation
import regroupement_indicateurs
from collections import Counter  # for computing evalutation metrics

excluded_profiles = ['000010',
                     # clustering affinity = ValueError: Found array with dim 3. check_pairwise_arrays expected <= 2
                     '000072',  # affinity prog = idem
                     '000011',
                     # spectral clustering = numpy.linalg.linalg.LinAlgError: 87-th leading minor not positive definite
                     '000012',  # idem
                     '000033',  # idem
                     '000142',  # idem
                     '000103',  # idem
                     '000089',  # birch / agglo clustering - ValueError: Found array with 1 sample(s) (shape=(1, 33))
                     '000031',  # ALTERNANT - zone de recherche trop grande (Lyon pour ecole mais work/alternance dans Vaucluse)
                     # ALTERNANT : 000152 (incomplet) et 000139 (incomplet)
                     '000020',  # iris 594370000 [ERROR] - shapely.geos - TopologyException: Input geom 1 is invalid: Self-intersection at or near point 2.1244298860372726 50.971212209886566 at 2.1244298860372726 50
                     '000080',  # iris 480490000 (from pyris) not in our data (for Chirac/Bourgs-sur-Calagne near Mende)
                     ]

def statistics_indicators(list_indicators):
    # compute statistics for a list of indicators (#indicators with value 0, #total indicators)
    values = list_indicators.values()
    return values.count(0), len(list_indicators)


def stats_profile_HiL():
    # for each profile, compares the three iris (minimal distance between iris_work and iris_arrival,
    # number of candidates,
    profiles_hil = json_utils.parse_json_to_dict(config.json_HiL_profils_from_to)
    neighborhoods = json_utils.parse_json_to_dict(config.json_neighborhoods_FR)

    nb_less_5kms = nb_5_10kms = nb_10_20kms = nb_more_20kms = 0  # ditance iris_work and iris_arrival
    nb_less_10_iris = nb_10_50_iris = nb_50_500_iris = nb_more_500_iris = 0  # nb candidate iris
    nb_valid_profiles = 0

    for profile_id, profile in profiles_hil.items():
        config.logger.info(profile_id + "\t" + str(profile))
        code_iris_start = profile["code_iris_start"]
        code_iris_work = profile["code_iris_work"]
        code_iris_arrival = profile["code_iris_arrival"]
        iris_start = geo_utils.get_iris_from_code_iris(code_iris_start)
        iris_work = geo_utils.get_iris_from_code_iris(code_iris_work)
        iris_arrival = geo_utils.get_iris_from_code_iris(code_iris_arrival)
        if iris_start is not None and iris_work is not None and iris_arrival is not None:
            nb_valid_profiles += 1
            dist = geo_utils.compute_min_distance_between_iris(iris_arrival["geometry"], iris_work["geometry"])
            if dist < 5.0:
                nb_less_5kms += 1
            elif dist < 10.0:
                nb_5_10kms += 1
            elif dist < 20.0:
                nb_10_20kms += 1
            else:
                nb_more_20kms += 1
            print "\t - distance(iris_work, iris_arrival) = " + str(round(dist, 1)) + " kms"
            '''
            stats_start = statistics_indicators(iris_start["properties"][config.geojson_grouped_indicators_label])
            print "\t - stats iris start (#0.0 / #total) = " + str(stats_start[0]) + " / " + str(stats_start[1])
            stats_work = statistics_indicators(iris_work["properties"][config.geojson_grouped_indicators_label])
            print "\t - stats iris work (#0.0 / #total) = " + str(stats_work[0]) + " / " + str(stats_work[1])
            stats_arrival = statistics_indicators(iris_arrival["properties"][config.geojson_grouped_indicators_label])
            print "\t - stats iris arrival (#0.0 / #total) = " + str(stats_arrival[0]) + " / " + str(stats_arrival[1])
            '''
            #iris_candidates = geo_utils.build_distant_neighborhood(code_iris_work, dist + 1.0)
            if config.geojson_profiles_ideal_neighbourhood_1km_label in neighborhoods[code_iris_work]:
                iris_candidates = neighborhoods[code_iris_work][config.geojson_profiles_ideal_neighbourhood_1km_label]
                nb_candidates = len(iris_candidates)
                if nb_candidates < 10:
                    nb_less_10_iris += 1
                elif nb_candidates < 50:
                    nb_10_50_iris += 1
                elif nb_candidates < 500:
                    nb_50_500_iris += 1
                else:
                    nb_more_500_iris += 1
                print "\t - #candidate iris = " + str(len(iris_candidates))
        else:
            print("\t ERROR - at least one iris not found (start, work, arrival) : ")
            print "\t " + str(iris_start)
            print "\t " + str(iris_work)
            print "\t " + str(iris_arrival)

    print "***********************************************"
    print "Stats for profiles - " + str(nb_valid_profiles) + " valid (start, work, and arrival iris filled)"
    print "# dist < 5 kms = " + str(nb_less_5kms)
    print "# 5 < dist < 10 kms = " + str(nb_5_10kms)
    print "# 10 < dist < 20 kms = " + str(nb_10_20kms)
    print "# dist > 20 kms = " + str(nb_more_20kms)
    print "# nb iris candidates < 10 = " + str(nb_less_10_iris)
    print "# 10 < nb iris candidates < 50 = " + str(nb_10_50_iris)
    print "# 50 < nb iris candidates < 500 = " + str(nb_50_500_iris)
    print "# nb iris candidates > 500 = " + str(nb_more_500_iris)


def expe_recommend_HiL(algorithms, use_neighborhood=False, topk=10):

    profiles_hil = json_utils.parse_json_to_dict(config.json_HiL_profils_from_to)
    neighborhoods = json_utils.parse_json_to_dict(config.json_neighborhoods_FR)
    results_per_profile = json_utils.parse_json_to_dict(config.results_expe_HiL)

    evaluation_metrics = dict()
    nb_valid_profiles = 0  # valid profiles have a start, work and arrival iris

    for algorithm in algorithms:
        evaluation_metrics[algorithm] = Counter({'tp': 0, 'fp': 0, 'fn': 0})

    for profile_id, profile in profiles_hil.items():
        if profile_id not in excluded_profiles:  # some errors with profiles, so exclude them
            config.logger.info(profile_id + "\t" + str(profile))

            if profile_id in results_per_profile and len(results_per_profile[profile_id]) == len(algorithms):  # results computed for this profile, retrieving the results unless empty
                nb_valid_profiles += 1
                for algo in algorithms:
                    evaluation_metrics[algo] = Counter(results_per_profile[profile_id][algo]) + evaluation_metrics[algo]

            else :  # profile_id not in results_per_profile
                results_per_profile[profile_id] = dict()
                # get iris objects (extract department, read iris and index file for the department, get position from index)
                code_iris_start = profile["code_iris_start"]
                code_iris_work = profile["code_iris_work"]
                code_iris_arrival = profile["code_iris_arrival"]
                iris_start = geo_utils.get_iris_from_code_iris(code_iris_start)
                iris_work = geo_utils.get_iris_from_code_iris(code_iris_work)
                iris_arrival = geo_utils.get_iris_from_code_iris(code_iris_arrival)

                if iris_start is not None and iris_work is not None and iris_arrival is not None \
                        and config.geojson_profiles_ideal_neighbourhood_1km_label in neighborhoods[code_iris_work]:  # recommend
                    nb_valid_profiles += 1

                    # computes a dynamic distance equal to (distance between iris_work and iris_arrival + 1 km)
                    # dynamic_distance = geo_utils.compute_min_distance_between_iris(iris_arrival["geometry"], iris_work["geometry"]) + 1.0
                    # iris_candidates = geo_utils.build_distant_neighborhood(code_iris_work, dynamic_distance)
                    iris_candidates = geo_utils.get_iris_from_codes_iris(
                        neighborhoods[code_iris_work][config.geojson_profiles_ideal_neighbourhood_1km_label])

                    list_features = iris_start["properties"][config.geojson_grouped_indicators_label].keys()

                    if use_neighborhood:  # get the grouped indicators of direct neighbors for iris start and iris candidates
                        iris_start = geo_utils.get_grouped_indicators_direct_neighbors([iris_start])[0]
                        iris_candidates = geo_utils.get_grouped_indicators_direct_neighbors(iris_candidates)

                        # this is for computing and storing grouped indi of direct neigbors
                        # list_codes = [iris["properties"]["CODE_IRIS"] for iris in iris_candidates] \
                        #             + [code_iris_start, code_iris_work, code_iris_arrival]
                        # print len(list_codes), list_codes
                        # geo_utils.compute_grouped_indicators_direct_neighbors(list_codes)

                    # cosine measure
                    if 'cosine' in algorithms:
                        recommendations, _ = recommender.recommend_cosine_measure(iris_start, iris_candidates,
                                                                                  list_features, topk)
                        # print recommendations
                        tp, fp, fn = evaluation.compute_tp_fp_fn(recommendations.keys(), [code_iris_arrival])
                        evaluation_metrics['cosine'] = Counter({'tp': tp, 'fp': fp, 'fn': fn}) + evaluation_metrics['cosine']
                        results_per_profile[profile_id]['cosine'] = {'tp': tp, 'fp': fp, 'fn': fn}

                    # standard deviation
                    if 'std' in algorithms:
                        # todo: prendre les iris voisins comme liste de start_iris (pour construire un profil) ?
                        recommendations = recommender.recommend_standard_deviation([iris_start], iris_candidates,
                                                                                   list_features, topk)
                        # print recommendations
                        tp, fp, fn = evaluation.compute_tp_fp_fn(recommendations, [code_iris_arrival])
                        evaluation_metrics['std'] = Counter({'tp': tp, 'fp': fp, 'fn': fn}) + evaluation_metrics['std']
                        results_per_profile[profile_id]['std'] = {'tp': tp, 'fp': fp, 'fn': fn}

                    # clustering (default k-means algorithm)
                    for algo in config.clustering_recommendation_algorithms:
                        if algo in algorithms:
                            recommendations, _, _ = recommender.recommend_clustering(iris_candidates, list_features,
                                                                                     iris_start, algorithm=algo, topk=topk)
                            # print recommendations
                            tp, fp, fn = evaluation.compute_tp_fp_fn(recommendations.keys(), [code_iris_arrival])
                            evaluation_metrics[algo] = Counter({'tp': tp, 'fp': fp, 'fn': fn}) + evaluation_metrics[algo]
                            results_per_profile[profile_id][algo] = {'tp': tp, 'fp': fp, 'fn': fn}

                    # todo problem for svm incorrect number of features - check if no vector is built from an iris without grped ind for neigbours

                    # todo: heuristic (not realistic) : svm needs 2 classes so iris_start and its direct neighbours are class 1,
                    # todo: while the level2_neighbors of iris_start are class 0 (not desired)

                    # svm
                    for algo in config.svm_recommendation_algorithms:
                        if algo in algorithms:
                            recommendations = recommender.recommend_svm(iris_start, iris_candidates, list_features,
                                                                        algorithm=algo)
                            # print recommendations
                            tp, fp, fn = evaluation.compute_tp_fp_fn(recommendations.keys(), [code_iris_arrival])
                            evaluation_metrics[algo] = Counter({'tp': tp, 'fp': fp, 'fn': fn}) + evaluation_metrics[algo]
                            results_per_profile[profile_id][algo] = {'tp': tp, 'fp': fp, 'fn': fn}

                #if nb_valid_profiles % 5 == 0:  # for testing/running experiments on a subset of the profiles and saving results
                    json_utils.save_dict_to_json(config.results_expe_HiL, results_per_profile)
                    #    break

    # print the overall results (sum of tp, sum of fp, sum of fn, and prec, rec, fmeas)
    print "*******************************************************************************"
    print "Experiment results (" + str(nb_valid_profiles) + " profiles):"
    for metric, results in evaluation_metrics.items():
        prec, rec, fmeas = evaluation.compute_prec_rec_fmeas_stats(results['tp'], results['fp'], results['fn'])
        print ("* " + metric + " : ").ljust(25) + str(dict(results)).ljust(35) + "\t\t{'prec': " + str(round(prec, 2)) \
              + ", 'rec': " + str(round(rec, 2)) + ", 'fmeas': " + str(round(fmeas, 2)) + "}"


def expe_regression(list_features, nb_clusters=10, cache_features=False):
    """
    Performs recommendation for all HiL profiles. First cluster all candidate candidate iris for all profiles
    (currently 7671 iris grouped into 10 clusters). Then
    :param list_features: the list of features (config.list_grouped_indicators or config.list_all_indicators)
    :param nb_clusters: the number of expected clusters for clustering algorithm
    :param cache_features: if True, the method first creates a new cache for all iris (vector of feature
    :return:
    """
    # utiliser adaboot regression to predict the cluster number for a K-fold and evaluate on the rest (ou recup les
    # poids de la regression avec feature_importances_

    # beurk ces imports mais juste pour des tests (pas de modif des autres fichiers)
    from sklearn.ensemble import AdaBoostRegressor
    from sklearn import cluster
    import numpy as np

    neighbordhood = config.geojson_profiles_ideal_neighbourhood_1km_label  # need another cache for 2kms

    profiles_hil = json_utils.parse_json_to_dict(config.json_HiL_profils_from_to)
    neighborhoods = json_utils.parse_json_to_dict(config.json_neighborhoods_FR)

    evaluation_metrics = Counter({'tp': 0, 'fp': 0, 'fn': 0})
    codes_iris = list()  # list of iris codes for candidates
    vectors_iris = list()  # list of vectors containing features (indicators) of each iris (same order than codes_iris)
    valid_id_profiles = list()

    if cache_features:  # need to compute and store iris codes and features (~1h execution)
        for profile_id, profile in profiles_hil.items():
            if profile_id not in excluded_profiles:  # some errors with profiles, so exclude them
                config.logger.info(profile_id + "\t" + str(profile))
                code_iris_start = profile["code_iris_start"]
                code_iris_work = profile["code_iris_work"]
                code_iris_arrival = profile["code_iris_arrival"]
                iris_start = geo_utils.get_iris_from_code_iris(code_iris_start)
                iris_work = geo_utils.get_iris_from_code_iris(code_iris_work)
                iris_arrival = geo_utils.get_iris_from_code_iris(code_iris_arrival)

                if iris_start is not None and iris_work is not None and iris_arrival is not None \
                        and neighbordhood in neighborhoods[code_iris_work]:  # add candidates
                    iris_candidates_codes = neighborhoods[code_iris_work][neighbordhood]
                    iris_candidates_codes = [item for item in iris_candidates_codes if item not in codes_iris]
                    iris_candidates = geo_utils.get_iris_from_codes_iris(iris_candidates_codes)
                    vectors_candidate, codes_candidate_iris = recommender.build_feature_vectors(iris_candidates, list_features)
                    codes_iris += codes_candidate_iris  # adding the new candidate iris for clustering
                    vectors_iris += vectors_candidate  # adding the new canddiate vectors for clustering
                    valid_id_profiles.append(profile_id)

        assert len(codes_iris) == len(vectors_iris), "Error: the number of candidate iris codes is different than the number of vectors"
        cache = dict()
        cache['valid_id_profiles'] = valid_id_profiles
        cache['codes_all_candidate_iris'] = codes_iris
        cache['vectors_all_candidate_iris'] = vectors_iris
        json_utils.save_dict_to_json(config.cache_expe_HiL_clustering_all_vectors_1km, cache)

    else:  # not computing vectors, extracting them from cache
        cache = json_utils.parse_json_to_dict(config.cache_expe_HiL_clustering_all_vectors_1km)  # 7671 iris
        valid_id_profiles = cache['valid_id_profiles']
        codes_iris = cache['codes_all_candidate_iris']
        vectors_iris = cache['vectors_all_candidate_iris']
    print "Extraction OK: codes and vectors of iris computed"

    # do clustering
    clusters_iris = cluster.KMeans(n_clusters=nb_clusters, random_state=0).fit(np.array(vectors_iris))
    #clusters_iris = cluster.Birch(n_clusters=nb_clusters).fit(np.array(vectors_iris))
    #clusters_iris = cluster.AgglomerativeClustering(n_clusters=nb_clusters).fit(np.array(vectors_iris))
    #clusters_iris = cluster.MiniBatchKMeans(n_clusters=nb_clusters).fit(np.array(vectors_iris))
    #clusters_iris = cluster.SpectralClustering(n_clusters=nb_clusters).fit(np.array(vectors_iris))
    print "Clustering OK : %d iris grouped into %d clusters" % (len(clusters_iris.labels_), nb_clusters)
    #print clusters_iris.labels_

    nb_no_neighbors_label = 0
    nb_arrival_not_in_neighbors = 0
    nb_arrival_in_neighbors = 0

    for profile_id, profile in profiles_hil.items():
        if profile_id not in excluded_profiles and profile_id in valid_id_profiles:  # exclude invalid or erroneous profiles
            #config.logger.info(profile_id + "\t" + str(profile))
            code_iris_start = profile["code_iris_start"]
            code_iris_work = profile["code_iris_work"]
            code_iris_arrival = profile["code_iris_arrival"]

            if code_iris_start != '' and code_iris_work != '' and code_iris_arrival != '':

                # todo delete later
                if neighbordhood in neighborhoods[code_iris_work]:
                    candidates_codes = neighborhoods[code_iris_work][neighbordhood]
                    if code_iris_arrival in candidates_codes:
                        nb_arrival_in_neighbors += 1
                    else:
                        config.logger.info("profile with arrival not in candidate_codes_iris (-> FN) : " + profile_id + "\t" + str(profile))
                        nb_arrival_not_in_neighbors += 1
                else:
                    config.logger.info("profil with no neigborhood_1km label : " + profile_id + "\t" + str(profile))
                    nb_no_neighbors_label += 1
                # todo end delete later

                iris_start = geo_utils.get_iris_from_code_iris(code_iris_start)
                if iris_start is not None:
                    vector_start_iris, _ = recommender.build_feature_vectors([iris_start], list_features)
                    cluster_id_predicted = clusters_iris.predict(vector_start_iris)[0]
                    # evaluation
                    if code_iris_arrival not in codes_iris:
                        config.logger.info("profile with arrival not in codes_iris : " + profile_id + "\t" + str(profile))
                        evaluation_metrics['fn'] += 1
                    else:
                        index = codes_iris.index(code_iris_arrival)
                        cluster_id_expected = clusters_iris.labels_[index]
                        if cluster_id_predicted == cluster_id_expected:
                            evaluation_metrics['tp'] += 1
                        else:
                            evaluation_metrics['fp'] += 1

    prec, rec, fmeas = evaluation.compute_prec_rec_fmeas_stats(evaluation_metrics['tp'], evaluation_metrics['fp'], evaluation_metrics['fn'])
    print "Experiment results (prec, recall, fmeas): \t %.2f \t %.2f \t %.2f" % (prec, rec, fmeas)
    print str(dict(evaluation_metrics))

    print "nb_no_neighbors_label = %d" % (nb_no_neighbors_label)
    print "nb_arrival_not_in_neighbors = %d" % (nb_arrival_not_in_neighbors)
    print "nb_arrival_in_neighbors = %d" % (nb_arrival_in_neighbors)

    # TODO should return the list of reco (for 1 profile, 1 reco if same cluster or empty list), or continue with Adaboost if needed
    # do regression with Kfold ou get the weights for all instances
    #regression = AdaBoostRegressor()





if __name__ == "__main__":
    algorithms = config.basic_recommendation_algorithms + config.homemade_recommendation_algorithms + \
                 config.clustering_recommendation_algorithms + ['one_class_svm']

    #expe_recommend_HiL(algorithms, use_neighborhood=False)
    #stats_profile_HiL()

    expe_regression(list_features=config.list_grouped_indicators)  # config.list_all_indicators


