#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# Main class for geocoding (retrieving an iris from coordinates or address)
# Check results using http://pyris.datajazz.io/ et http://pyris.datajazz.io/api/
# =============================================================================

import urllib3
import json

# TODO
# get_code_iris_from_address(url) -> not used except for transforming HiL data profiles (adresses) into iris codes


def build_url_from_address(address):
    address_clean = address.replace(' ', '%20')  # replace all spaces by %20 for url parameter
    request = "http://pyris.datajazz.io/api/search?q=" + str(address_clean)
    return request


def send_request(url):
    http = urllib3.PoolManager()
    response = http.request('GET', url)
    #if response.status == '200':  # valid url
    dict = json.loads(response.data.decode('utf-8'))
    return dict


def get_code_iris_from_address(url):
    try:
        response = send_request(url)
        res_iris = str(response['complete_code'])
    except Exception as e:
        res_iris = None
    print("Found IRIS : " + str(res_iris))
    return res_iris


if __name__ == "__main__":
    url_test = build_url_from_address('2 rue de nuits 69004 Lyon')
    assert (get_code_iris_from_address(url_test) == '693840202'), 'Address "2 rue de nuits 69004 Lyon" should correspond to IRIS 693840202'

