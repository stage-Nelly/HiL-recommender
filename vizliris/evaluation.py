#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# Script for evaluating experiments using quality metrics (precision, recall, etc.)
# =============================================================================



def compute_tp_fp_fn(algo_recommendations, ground_truth_recommendations):
    # computes true pos., false pos. and false neg. between recommendations from an algorithm and ground truth recommendations
    true_positives = len(set(algo_recommendations) & set(ground_truth_recommendations))
    false_positives = len(set(algo_recommendations) - set(ground_truth_recommendations))
    false_negatives = len(set(ground_truth_recommendations) - set(algo_recommendations))
    return true_positives, false_positives, false_negatives


def compute_prec_rec_fmeas_sets(algo_recommendations, ground_truth_recommendations):
    # computes precision, recall and fmeasure between recommendations from an algorithm and ground truth recommendations
    tp, fp, fn = compute_tp_fp_fn(algo_recommendations, ground_truth_recommendations)
    return compute_prec_rec_fmeas_stats(tp, fp, fn)


def compute_prec_rec_fmeas_stats(tp, fp, fn):
    # computes precision, recall and fmeasure between recommendations from true pos., false pos. and false neg.
    if (tp + fp) > 0:
        prec = (1.0 * tp) / (tp + fp)
    else:
        prec = 0.0
    if (tp + fn) > 0:
        rec = (1.0 * tp) / (tp + fn)
    else:
        rec = 0.0
    if prec == 0.0 or rec == 0.0:
        fmeas = 0.0
    else:
        fmeas = (2.0 * prec * rec) / (prec + rec)
    return prec, rec, fmeas




