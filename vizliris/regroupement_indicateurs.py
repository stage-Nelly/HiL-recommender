import json
import json_utils
import config

def compute_grouped_indicators(dict_raw_indicators, dict_grouping_indicators):
    dict_grouped_indicators_values = {}  # dictionnaire qui contient les valeurs des indicateurs regroupes

    valeursInconnues = []
    for i in dict_grouping_indicators:  # categorie (e.g. animation.divertissement)
        categorie = dict_grouping_indicators[i]
        for j in categorie:  # operateur
            nombreAdd = 0
            nombreMult = 1
            nombreDivAdd = nombreDivMult = float(0)
            operande1Add = operande2Add = float(0)
            operande1Mult = operande2Mult = 1
            indicateur = categorie[j]
            ajouter = 0.0

            # on traite la division avant de rentrer dans la boucle sur k car c'est un cas special (elle a un niveau de plus)
            if j == "/":
                # on considere la division comme : (operateur1 applique a tous les operandes du haut) / (operateur2 applique a tous les operandes du bas)
                for m, n in zip(categorie[j]["op1"], categorie[j]["op2"]):
                    if m == "+":
                        for p in categorie[j]["op1"]["+"]:
                            # on ajoute les indicateurs seulement s'ils existent
                            if not (str(p) in dict_raw_indicators):
                                valeursInconnues.append(str(p))
                            else:
                                operande1Add += dict_raw_indicators[str(p)]  # on somme tous les operandes du haut
                        if n == "+":
                            for q in categorie[j]["op2"]["+"]:
                                if not (str(q) in dict_raw_indicators):
                                    valeursInconnues.append(str(q))
                                else:
                                    operande2Add += dict_raw_indicators[
                                        str(q)]  # on somme tous les operandes du bas
                            if operande2Add != 0:
                                nombreDivAdd = operande1Add / operande2Add
                                ajouter = nombreDivAdd
                        if n == "*":
                            for q in categorie[j]["op2"]["*"]:
                                if not (str(q) in dict_raw_indicators):
                                    valeursInconnues.append(str(q))
                                else:
                                    operande2Mult *= dict_raw_indicators[
                                        str(q)]  # on multiplie tous les operandes du bas
                            if operande2Mult != 0:
                                nombreDivMult = operande1Add / operande2Mult
                                ajouter = nombreDivMult
                    elif m == "*":
                        for p in categorie[j]["op1"]["*"]:
                            if not (str(p) in dict_raw_indicators):
                                valeursInconnues.append(str(p))
                            else:
                                operande1Mult *= dict_raw_indicators[
                                    str(p)]  # on multiplie tous les operandes du haut
                        if n == "*":
                            for q in categorie[j]["op2"]["*"]:
                                if not (str(q) in dict_raw_indicators):
                                    valeursInconnues.append(str(q))
                                else:
                                    operande2Mult *= dict_raw_indicators[
                                        str(q)]  # on multiplie tous les operandes du bas
                            if operande2Mult != 0:
                                nombreDivMult = operande1Mult / operande2Mult
                                ajouter = nombreDivMult
                        if n == "+":
                            for q in categorie[j]["op2"]["+"]:
                                if not (str(q) in dict_raw_indicators):
                                    valeursInconnues.append(str(q))
                                else:
                                    operande2Mult += dict_raw_indicators[
                                        str(q)]  # on somme tous les operandes du bas
                            if operande2Mult != 0:
                                nombreDivMult = operande1Mult / operande2Add
                                ajouter = nombreDivMult
                    else:
                        print("operateur non reconnu")
            else:
                for k in indicateur:  # indicateur (e.g. NB_AXXX)
                    if j == "+":
                        if not (str(k) in dict_raw_indicators):
                            valeursInconnues.append(str(k))
                        else:
                            nombreAdd += dict_raw_indicators[k]  # on somme tous les indicateurs
                            ajouter = nombreAdd
                    elif j == "*":
                        if not (str(k) in dict_raw_indicators):
                            valeursInconnues.append(str(k))
                        else:
                            nombreMult *= dict_raw_indicators[k]  # on multiplie tous les indicateurs
                            ajouter = nombreMult
                    else:
                        print("Operateur inconnu")
        # on ajoute les valeurs dans le dictionnaire
            dict_grouped_indicators_values[str(i)] = ajouter
    # if valeursInconnues != []:
    # 	print(valeursInconnues)
    # else:
    # 	print("OK")

    return dict_grouped_indicators_values


def index():
    with open('static/data/iris-69.geojson') as iris:
        all_iris = json.load(iris)

    dict_grouping_indicators = json_utils.parse_json_to_dict(config.grouping_indicators_file)

    dict_valeurs = {}
    for iris in all_iris["features"]:
        if config.geojson_indicators_label in iris["properties"]:
            code_iris = iris["properties"]["CODE_IRIS"]
            dict_valeurs[code_iris] = compute_grouped_indicators(iris["properties"][config.geojson_indicators_label],
                                                                 dict_grouping_indicators)

    print(json.dumps(dict_valeurs, indent=2))


if __name__ == "__main__":
    index()
